@prefix fresnel:  <http://www.w3.org/2004/09/fresnel#> .
@prefix rdf: 		<http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: 		<http://www.w3.org/2000/01/rdf-schema#> .
@prefix owl: 		<http://www.w3.org/2002/07/owl#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix dc: 		<http://purl.org/dc/elements/1.1/#> .
@prefix xml: 		<http://www.w3.org/XML/1998/namespace> .
@prefix vann: 		<http://purl.org/vocab/vann/> .
@prefix qb:       <http://purl.org/linked-data/cube#> .
@prefix wot: 		<http://xmlns.com/wot/0.1/> .
@prefix dct: 		<http://purl.org/dc/terms/> .
@prefix void: 		<http://rdfs.org/ns/void#> .
@prefix row: 		<https://www.rbv.minfin.nl/2020/modellen/begroting/id/row/> .
@prefix pres: 		<https://www.rbv.minfin.nl/modellen/> .
@prefix : 		<https://www.rijksfinancien.nl/informatieproducten/def/> .
@prefix cell: 		<https://www.rbv.minfin.nl/2020/modellen/begroting/id/cell/> .
@prefix vs: 		<http://www.w3.org/2003/06/sw-vocab-status/ns#> .
@prefix foaf: 		<http://xmlns.com/foaf/0.1/> .
@prefix sdo: <https://schema.org/> .
@prefix id: 		<https://www.rijksfinancien.nl/informatieproducten/id/> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .
@prefix dcam: <http://purl.org/dc/dcam/> .
@prefix dcterms: <http://purl.org/dc/terms/> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .


<https://www.rijksfinancien.nl/informatieproducten/def/> {
 <https://www.rijksfinancien.nl/informatieproducten/def/> rdf:type owl:Ontology ;
                                                          dc:title "PresentatieOntologie"@nl;
                                                          dc:description "Ontologie die de modellen van de overheid met betrekking van Financien raadsbeslagingen in linked data modelleerd."@nl ;
                                                          dct:created "2020-07-14"^^xsd:date ;
                                                          dct:title "Presentation Dataset"@en ;
                                                          dct:description "The Presentation ontology that specifies a RDF data model for the fundamental elements for information display."@en ;
                                                          vann:preferredNamespacePrefix "present" ;
                                                          void:subset  <https://www.rijksfinancien.nl/informatieproducten/def/vocabulary>;
                                                          void:subset <https://www.rijksfinancien.nl/informatieproducten/exampleDataset>;
                                                          dct:status <http://purl.org/adms/status/UnderDevelopment> .

 <https://www.rijksfinancien.nl/informatieproducten/def/vocabulary> rdf:type owl:Ontology ;
                                                                    dc:title "Presentatie Ontologie"@nl;
                                                                    dc:description "Ontologie die het rijks presentatie model vastlegt in Linked data."@nl ;
                                                                    dct:created "2020-07-14"^^xsd:date ;
                                                                    dct:title "Presentation Ontology"@en ;
                                                                    dct:description "The Presentation ontology that specifies a RDF data model for RBV in Linked data."@en ;
                                                                    vann:preferredNamespacePrefix "present" ;
                                                                    dct:status <http://purl.org/adms/status/UnderDevelopment> .


}
#################################################################

<https://www.rijksfinancien.nl/informatieproducten/def/vocabulary> {

  :heeftContext rdf:type owl:ObjectProperty ;
                  rdfs:label "Heeft Context"@nl;
                  rdfs:label "Has Context"@en;
                  rdfs:comment "De relatie tussen het informatie product en de context van het informatie product"@nl;
                  rdfs:comment "The relation between the context of the informationproduct and the informationproduct."@en;
                  rdfs:domain :InformatieProduct ;
                  rdfs:range :InformatieProductContext .

 :heeftInformatieProductType rdf:type owl:ObjectProperty;
                             rdfs:label "Heeft InformatieProductType"@nl;
                             rdfs:label "Has InformatieProduct Type"@en;
                             rdfs:comment "Heeft een InformatieProduct, beschrijft het InformatieProductType, e.g. tabel etc."@nl;
                             rdfs:comment "Has modelType, describes the modelType of the informationProduct, e.g. tabel etc."@en;
                             rdfs:range :InformatieProduct ;
                             rdfs:domain :InformatieProductType .

  :modelType rdf:type owl:ObjectProperty;
             rdfs:label "Heeft modelType"@nl;
             rdfs:label "Has modelType"@en;
             rdfs:comment "Heeft een modeltype, beschrijft het modeltype van het informatieProduct."@nl;
             rdfs:comment "Has modelType, describes the modelType of the informationProduct."@en;
             rdfs:range :ModelType ;
             rdfs:domain :InformatieProductContext .

  :heeftToelichting rdf:type owl:DatatypeProperty ;
                    rdfs:label "Heeft toelichting"@nl;
                    rdfs:label "Has explanation"@en;
                    rdfs:comment "Beschrijft en licht het informatieProduct toe, voornamelijk over hoe het moet worden ingevuld."@nl;
                    rdfs:comment "Explaination on how to use and fill the informationProduct with instantiated information."@en;
                    rdfs:domain :InformatieProductContext ;
                    rdfs:range rdf:HTML .

  :heeftDataStructuurDefinition rdf:type owl:ObjectProperty ;
                                rdfs:label "Heeft datastructuurdefinitie"@nl;
                                rdfs:label "Has DataStructureDefinition"@en;
                                rdfs:comment "De link met de Datastructuurdefinitie die de slice beschrijft van het informatie product."@nl;
                                rdfs:comment "Datastructure definition that describes the input of the informaiton product"@en;
                                rdfs:domain :InformatieProduct ;
                                rdfs:range qb:DataStructureDefinition .

  :heeftSPARQLQuery rdf:type owl:DatatypeProperty ;
                                rdfs:label "Heeft SPARQL query"@nl;
                                rdfs:label "Has SPARQL query"@en;
                                rdfs:comment "Beschrijft hoe het Informatie product opgebouwd is doormiddel van een SPARQL query, dit levert herbruikbaarheid op van de query end het product."@nl;
                                rdfs:comment "the SPARQL query is explaination on how the informationProduct is created."@en;
                                rdfs:domain :InformatieProduct ;
                                rdfs:range rdfs:Literal .

  :heeftSPARQLContructQuery rdf:type owl:DatatypeProperty ;
                            rdfs:label "Heeft SPARQL query"@nl;
                            rdfs:label "Has SPARQL query"@en;
                            rdfs:comment "Beschrijft hoe het Informatie product opgebouwd is doormiddel van een SPARQL query, dit levert herbruikbaarheid op van de query end het product."@nl;
                            rdfs:comment "the SPARQL query is explaination on how the informationProduct is created."@en;
                            rdfs:range rdfs:Literal .

  :heeftOpmaak rdf:type owl:ObjectProperty ;
               rdfs:label "Heeft opmaak"@nl;
               rdfs:label "Has design"@en;
               rdfs:comment "The relatie tussen een informatieProduct en de opmaak van het informatieProduct"@nl;
               rdfs:comment "The relation between the design of the informationProduct and the informationProduct"@en;
               rdfs:domain :InformatieProduct ;
               rdfs:range fresnel:Group .

  :heeftIndeling rdf:type owl:ObjectProperty ;
                 rdfs:label "Heeft indeling"@nl;
                 rdfs:label "Has layout"@en;
                 rdfs:comment "Een informatieproduct dat uit meerdere onderdelen bestaat heeft een indeling over hoe de onderdelen zich tot de andere onderdelen verhouden."@nl;
                 rdfs:comment "The relation between the layout of the informationProduct parts and the encompassing InformationProduct."@en;
                 rdfs:domain :InformatieProduct ;
                 rdfs:range :Indeling .

  :InformatieProductContext rdf:type owl:Class ;
             rdfs:subClassOf :InformatieProduct ;
             rdfs:label "Context van het InformatieProduct"@nl;
             rdfs:label "Context of the InformationProduct"@en;
             rdfs:comment "De formele Context van het informatieProduct."@nl;
             rdfs:comment "The formal Context of the informationProduct, what the information product mean."@en .


  :InformatieProduct rdfs:subClassOf dcterms:BibliographicResource, sdo:CreativeWork ;
                     rdf:type owl:Class ;
                     rdfs:label "InformatieProduct"@nl;
                     rdfs:label "InformationProduct"@en;
                     rdfs:comment "Het informatie product dat door de RBV zijn voorgeschreven"@nl;
                     rdfs:comment "The informationProduct, that has been detailed by the RBV."@en .

  :Indeling rdf:type owl:Class ;
            rdfs:subClassOf rdf:List ;
            rdfs:label "Indeling van informatieproduct onderdelen"@nl;
            rdfs:label "Layout of information product parts"@en;
            rdfs:comment "Sommige informatie producten bestaan uit verschillende kleinere informatieproducten die gezamelijk een informatie product vormen."@nl;
            rdfs:comment "Some information products consist out of one or multiple smaller information products that combine into a large information product. They are ordered in the Layout."@en.

  :InformatieProductType rdf:type owl:Class ;
                         rdfs:label "InformatieProductType"@nl;
                         rdfs:label "InformatieProductType"@en;
                         rdfs:comment "Onderdelen van een InformationProduct kunnen worden onderverdeeld in een InformatieProductTypes"@nl;
                         rdfs:comment "parts of an informationproducts are divided into InformatieProductTypes."@en.

  :ModelType rdf:type owl:Class ;
             rdfs:label "ModelType"@nl;
             rdfs:label "ModelType"@en;
             rdfs:comment "InformationProducten kunnen worden onderverdeeld in een modeltype"@nl;
             rdfs:comment "Informationproducts are divided into modeltypes."@en.

  dcterms:abstract rdfs:domain :InformatieProduct .

  dcterms:accessRights rdfs:domain :InformatieProduct .

  dcterms:available rdfs:domain :InformatieProduct .

  dcterms:conformsTo rdfs:domain :InformatieProduct .

  dcterms:contributor rdfs:domain :InformatieProduct .

  dcterms:created rdfs:domain :InformatieProduct .

  dcterms:creator rdfs:domain :InformatieProduct .

  dcterms:date rdfs:domain :InformatieProduct .

  dcterms:dateAccepted rdfs:domain :InformatieProduct .

  dcterms:dateCopyrighted rdfs:domain :InformatieProduct .

  dcterms:dateSubmitted rdfs:domain :InformatieProduct .

  dcterms:description rdfs:domain :InformatieProduct .

  dcterms:format rdfs:domain :InformatieProduct .

  dcterms:hasFormat rdfs:range :InformatieProduct ;
                    rdfs:domain :InformatieProduct .

  dcterms:hasPart rdfs:range :InformatieProduct ;
                  rdfs:domain :InformatieProduct .

  dcterms:hasVersion rdfs:range :InformatieProduct ;
                     rdfs:domain :InformatieProduct .

  dcterms:identifier rdfs:domain :InformatieProduct .

  dcterms:isFormatOf  rdfs:range :InformatieProduct ;
                      rdfs:domain :InformatieProduct .

  dcterms:isPartOf rdfs:range :InformatieProduct ;
                   rdfs:domain :InformatieProduct .

  dcterms:isReferencedBy rdfs:range :InformatieProduct ;
                         rdfs:domain :InformatieProduct .

  dcterms:isReplacedBy rdfs:range :InformatieProduct ;
                       rdfs:domain :InformatieProduct .

  dcterms:isVersionOf rdfs:range :InformatieProduct ;
                      rdfs:domain :InformatieProduct .

  dcterms:issued rdfs:domain :InformatieProduct .

  dcterms:language rdfs:domain :InformatieProduct .

  dcterms:license rdfs:domain :InformatieProduct .

  dcterms:mediator rdfs:domain :InformatieProduct .

  dcterms:modified rdfs:domain :InformatieProduct .

  dcterms:provenance rdfs:domain :InformatieProduct .

  dcterms:publisher rdfs:domain :InformatieProduct .

  dcterms:references rdfs:range :InformatieProduct ;
                     rdfs:domain :InformatieProduct .

  dcterms:relation rdfs:range :InformatieProduct ;
                   rdfs:domain :InformatieProduct .

  dcterms:replaces rdfs:domain :InformatieProduct ;
                   rdfs:range :InformatieProduct .

  dcterms:requires rdfs:domain :InformatieProduct .

  dcterms:rights rdfs:domain :InformatieProduct .

  dcterms:rightsHolder rdfs:domain :InformatieProduct .

  dcterms:source rdfs:domain :InformatieProduct .

  dcterms:subject rdfs:domain :InformatieProduct .

  dcterms:tableOfContents rdfs:domain :InformatieProduct .

  dcterms:temporal rdfs:domain :InformatieProduct .

  dcterms:title rdfs:domain :InformatieProduct .

  dcterms:type rdfs:domain :InformatieProduct .

  dcterms:valid rdfs:domain :InformatieProduct .

}
