fin:BMUT70761 rdf:type fin:BudgetAdjustment;
	fin:hasBudgetFactNomenclature fin:BN70761  ;
	fin:hasNationalBudgetFact fin:BMUT70761-1.
fin:BN70761 fin:hasBudgetDomainSector fin:NationalBudgetNarrowSense  ;
	fin:hasBudgetaryApplicationEffect fin:GeneralBudgetaryApplication  ;
	fin:hasBudgetingSourceDocument fin:AdoptedSecondSuppletoryBill  ;
	fin:hasCategoryOfBudgetaryMutation fin:NotApplicable  ;
	fin:hasCauseOfBudgetaryMutation fin:PolicyBased  ;
	fin:hasDegreeOfCertainty fin:Certainty  ;
	fin:hasBudgetStructureReference fin:BSR70761  ;
	fin:hasBudgetMemorandumPeriod fin:BM2013 .
fin:BSR70761 fin:referencesBudgetStructureElement fin:BASI601002013  ;
	fin:referencesBudgetPolicyObjective fin:NotOtherwiseSpecified  .

fin:BMUT70761-1 rdf:type fin:NationalBudgetFact  ;
	fin:hasMonetaryAmount fin:MA70761  ;
	fin:hasTransactionalDirection fin:Outgoing  ;
	fin:inheritsBudgetFactNomenclature fin:BN70761  ;
	fin:hasLegalFramework fin:LF70761  ;
	fin:hasNationalBudgetPeriod fin:BP2013  ;
fin:MA70761 fin:hasCurrency fin:EUR  ;
	fin:hasValue '0'^^xsd:decimal  .

fin:BP2013 time:hasDateTimeDescription fin:BD2013  ;
	time:hasBeginning fin:BB2013  ;
	time:hasEnd fin:BE2013  .

fin:BM2013 time:hasDateTimeDescription fin:BD2013  ;
	time:hasBeginning fin:BB2013  ;
	time:hasEnd fin:BE2013  .

fin:BB2013 time:inXSDDate '2013-01-01'  .
fin:BE2013 time:inXSDDate '2013-12-31'  .
fin:BD2013 time:unitType time:unitYear  ;
	time:year '2013'^^xsd:gYear





