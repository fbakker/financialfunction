import { Ratt, CliContext } from "@triply/ratt"
import { mapValues } from "lodash"
import mw from "@triply/ratt/lib/middlewares"

export default async function (cliContext: CliContext): Promise<Ratt> {
  const prefix = Ratt.prefixer("https://www.rijksfinancien.nl/")
  const bnode = prefix(".well-known/genid/")
  const app = new Ratt({
    cliContext,
    defaultGraph: "https://www.rijksfinancien.nl/graph/model",
    prefixes: {
      // Externe namespaces
      "adms-assetType": Ratt.prefixer("http://purl.org/adms/assettype/"),
      "adms-status": Ratt.prefixer("http://purl.org/adms/status/"),
      bibo: Ratt.prefixer("http://purl.org/ontology/bibo/"),
      con: Ratt.prefixer("http://www.w3.org/2000/10/swap/pim/contact#"),
      dcm: Ratt.prefixer("http://purl.org/dc/dcmitype/"),
      dct: Ratt.prefixer("http://purl.org/dc/terms/"),
      def: Ratt.prefixer(prefix("model/def/")),
      deo: Ratt.prefixer("http://purl.org/spar/deo/"),
      doco: Ratt.prefixer("http://purl.org/spar/doco/"),
      fabio: Ratt.prefixer("http://purl.org/spar/fabio/"),
      foaf: Ratt.prefixer("http://xmlns.com/foaf/0.1/"),
      format: Ratt.prefixer("http://www.w3.org/ns/formats/"),
      frbr: Ratt.prefixer("http://purl.org/vocab/frbr/core#"),
      orb: Ratt.prefixer("http://purl.org/orb/1.0/"),
      org: Ratt.prefixer("http://www.w3.org/ns/org#"),
      owl: Ratt.prefixer("http://www.w3.org/2002/07/owl#"),
      po: Ratt.prefixer("http://www.essepuntato.it/2008/12/pattern#"),
      qb: Ratt.prefixer("http://purl.org/linked-data/cube#"),
      rdf: Ratt.prefixer("http://www.w3.org/1999/02/22-rdf-syntax-ns#"),
      rdfs: Ratt.prefixer("http://www.w3.org/2000/01/rdf-schema#"),
      sdo: Ratt.prefixer("https://schema.org/"),
      skos: Ratt.prefixer("http://www.w3.org/2004/02/skos/core#"),
      ssd: Ratt.prefixer("http://www.w3.org/ns/sparql-service-description#"),
      swap: Ratt.prefixer("http://www.w3.org/2000/10/swap/pim/doc#"),
      time: Ratt.prefixer("http://www.w3.org/2006/time#"),
      vann: Ratt.prefixer("http://purl.org/vocab/vann/"),
      vs: Ratt.prefixer("http://www.w3.org/2003/06/sw-vocab-status/ns#"),
      wgs84: Ratt.prefixer("http://www.w3.org/2003/01/geo/wgs84_pos#"),

      // Interne namespaces
      bnode: Ratt.prefixer(bnode),
      comp: Ratt.prefixer("https://www.rijksfinancien.nl/rijksbegroting/def/"),
      const: Ratt.prefixer("https://www.rijksfinancien.nl/grondwet/def/"),
      fcyc: Ratt.prefixer("https://www.rijksfinancien.nl/rijksbegrotingscyclus/def/"),
      ffact: Ratt.prefixer("https://www.rijksfinancien.nl/financieelfeit/def/"),
      fiad: Ratt.prefixer("https://www.rijksfinancien.nl/financieleadministratie/def/"),
      fstr: Ratt.prefixer("https://www.rijksfinancien.nl/rijksbegrotingsstructuur/def/"),
      ftext: Ratt.prefixer("https://www.rijksfinancien.nl/financieleteksten/def/"),
      ftype: Ratt.prefixer("https://www.rijksfinancien.nl/financieelfeittype/def/"),
      graph: Ratt.prefixer(prefix("graph/")),
      grondwet: Ratt.prefixer("https://www.rijksfinancien.nl/grondwet/def/"),
      pres: Ratt.prefixer("https://www.rijksfinancien.nl/informatieproducten/def/"),
    },
    sources: {
      // generiek (TriplyDB)
      enrich_definitions: Ratt.Source.TriplyDb.query("data-enrichment", "enrich-definitions", {triplyDb: {url: "https://api.triplydb.com"}}),
      enrich_lists: Ratt.Source.TriplyDb.query("data-enrichment", "enrich-lists", {triplyDb: {url: "https://api.triplydb.com"}}),
      enrich_owl: Ratt.Source.TriplyDb.query("data-enrichment", "enrich-owl", {triplyDb: {url: "https://api.triplydb.com"}}),
      enrich_node_shapes: Ratt.Source.TriplyDb.query("data-enrichment", "enrich-node-shapes", {triplyDb: {url: "https://api.triplydb.com"}}),
      enrich_property_shapes: Ratt.Source.TriplyDb.query("data-enrichment", "enrich-property-shapes", {triplyDb: {url: "https://api.triplydb.com"}}),
      enrich_sh_lex_1: Ratt.Source.TriplyDb.query("data-enrichment", "enrich-sh-lex-1", {triplyDb: {url: "https://api.triplydb.com"}}),
      enrich_sh_lex_2: Ratt.Source.TriplyDb.query("data-enrichment", "enrich-sh-lex-2", {triplyDb: {url: "https://api.triplydb.com"}}),
      enrich_skos_lex: Ratt.Source.TriplyDb.query("data-enrichment", "enrich-skos-lex", {triplyDb: {url: "https://api.triplydb.com"}}),
      enrich_value_lists: Ratt.Source.TriplyDb.query("data-enrichment", "enrich-value-lists", {triplyDb: {url: "https://api.triplydb.com"}}),
      extra_validate: Ratt.Source.TriplyDb.rdf("generic", "extra-validate", {triplyDb: {url: "https://api.triplydb.com"}}),
      generic_model: Ratt.Source.TriplyDb.rdf("generic", "model", {triplyDb: {url: "https://api.triplydb.com"}}),
      // specifiek (MinFin)
      model: Ratt.Source.file(["ttlOntologies/Budget.ttl",
                               "ttlOntologies/BudgetingCycle.ttl",
                               "ttlOntologies/Comptabiliteitswet.ttl",
                               "ttlOntologies/NationalBudgetProcess.ttl",
                               "ttlOntologies/OntologyBudgetPolicyAndTheme.ttl",
                               "ttlOntologies/OntologyBudgetStructureElements.ttl",
                               "ttlOntologies/OntologyFinancialFact.ttl",
                               "ttlOntologies/OntologyFinancialFactType.ttl",
                               "ttlOntologies/OntologyNationalBudgetFact.ttl",
                               "ttlOntologies/presentationOntologyFull.ttl",
                               "ttlOntologies/Rijksbegrotingsvoorschriften-Data.ttl",
                               "ttlOntologies/Rijksbegrotingsvoorschriften-Model.ttl"]),
    },
    destinations: {
      remote: Ratt.Destination.TriplyDb.rdf("FinancialFunction",
                                            "model",
                                            {synchronizeServices: true,
                                             truncateGraphs: true}),
    },
    wellKnownIriPrefix: bnode.value,
  })
  app.use(mw.fromRdf(app.sources.model))
  // Verrijking
  app.use(mw.sparqlConstruct(app.sources.enrich_owl, {toGraph: app.prefix.graph("model")}))
  app.use(mw.sparqlConstruct(app.sources.enrich_value_lists, {toGraph: app.prefix.graph("model")}))
  app.use(mw.sparqlConstruct(app.sources.enrich_skos_lex, {toGraph: app.prefix.graph("model")}))
  app.use(mw.sparqlConstruct(app.sources.enrich_node_shapes, {toGraph: app.prefix.graph("model")}))
  app.use(mw.sparqlConstruct(app.sources.enrich_property_shapes, {toGraph: app.prefix.graph("model")}))
  app.use(mw.sparqlConstruct(app.sources.enrich_sh_lex_1, {toGraph: app.prefix.graph("model")}))
  app.use(mw.sparqlConstruct(app.sources.enrich_sh_lex_2, {toGraph: app.prefix.graph("model")}))
  app.use(mw.sparqlConstruct(app.sources.enrich_lists, {toGraph: app.prefix.graph("model")}))
  app.use(mw.sparqlConstruct(app.sources.enrich_definitions, {toGraph: app.prefix.graph("model")}))
  // Validatie
  app.use(mw.fromRdf(app.sources.extra_validate))
  app.use(mw.validateShacl(app.sources.generic_model,
                           {report: {destination: app.destinations.remote,
                                     graph: app.prefix.graph("report")},
                            terminateOn: false}))
  // Context (LOD Cloud)
  app.use(mw.fromRdf(Ratt.Source.TriplyDb.rdf("dcmi", "dct", {triplyDb: {url: "https://api.triplydb.com"}}), {defaultGraph: app.prefix.dct("")}))
  app.use(mw.fromRdf(Ratt.Source.TriplyDb.rdf("ifla", "frbr", {triplyDb: {url: "https://api.triplydb.com"}}), {defaultGraph: app.prefix.frbr("")}))
  app.use(mw.fromRdf(Ratt.Source.TriplyDb.rdf("none", "foaf", {triplyDb: {url: "https://api.triplydb.com"}}), {defaultGraph: app.prefix.foaf("")}))
  app.use(mw.fromRdf(Ratt.Source.TriplyDb.rdf("none", "sdo", {triplyDb: {url: "https://api.triplydb.com"}}), {defaultGraph: app.prefix.sdo("")}))
  app.use(mw.fromRdf(Ratt.Source.TriplyDb.rdf("spar", "deo", {triplyDb: {url: "https://api.triplydb.com"}}), {defaultGraph: app.prefix.deo("")}))
  app.use(mw.fromRdf(Ratt.Source.TriplyDb.rdf("spar", "doco", {triplyDb: {url: "https://api.triplydb.com"}}), {defaultGraph: app.prefix.doco("")}))
  app.use(mw.fromRdf(Ratt.Source.TriplyDb.rdf("spar", "fabio", {triplyDb: {url: "https://api.triplydb.com"}}), {defaultGraph: app.prefix.fabio("")}))
  app.use(mw.fromRdf(Ratt.Source.TriplyDb.rdf("spar", "po", {triplyDb: {url: "https://api.triplydb.com"}}), {defaultGraph: app.prefix.po("")}))
  app.use(mw.fromRdf(Ratt.Source.TriplyDb.rdf("structured-dynamics", "bibo", {triplyDb: {url: "https://api.triplydb.com"}}), {defaultGraph: app.prefix.bibo("")}))
  app.use(mw.fromRdf(Ratt.Source.TriplyDb.rdf("w3c", "owl", {triplyDb: {url: "https://api.triplydb.com"}}), {defaultGraph: app.prefix.owl("")}))
  app.use(mw.fromRdf(Ratt.Source.TriplyDb.rdf("w3c", "qb", {triplyDb: {url: "https://api.triplydb.com"}}), {defaultGraph: app.prefix.qb("")}))
  app.use(mw.fromRdf(Ratt.Source.TriplyDb.rdf("w3c", "rdf", {triplyDb: {url: "https://api.triplydb.com"}}), {defaultGraph: app.prefix.rdf("")}))
  app.use(mw.fromRdf(Ratt.Source.TriplyDb.rdf("w3c", "rdfs", {triplyDb: {url: "https://api.triplydb.com"}}), {defaultGraph: app.prefix.rdfs("")}))
  app.use(mw.fromRdf(Ratt.Source.TriplyDb.rdf("w3c", "skos", {triplyDb: {url: "https://api.triplydb.com"}}), {defaultGraph: app.prefix.skos("")}))
  app.use(mw.fromRdf(Ratt.Source.TriplyDb.rdf("w3c", "swap", {triplyDb: {url: "https://api.triplydb.com"}}), {defaultGraph: app.prefix.swap("")}))
  app.use(mw.fromRdf(Ratt.Source.TriplyDb.rdf("w3c", "wgs84", {triplyDb: {url: "https://api.triplydb.com"}}), {defaultGraph: app.prefix.wgs84("")}))
  // Publicatie
  app.use(mw.toRdf(app.destinations.remote))
  app.after(async () => {
    const organization = await app.triplyDb.getOrganization("FinancialFunction")
    const dataset = organization.getDataset("model")
    await dataset.addDatasetPrefixes(mapValues(app.prefix, (prefix) => prefix("").value))
  })
  return app
}
