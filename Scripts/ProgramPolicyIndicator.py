# -*- coding: utf-8 -*-
"""
Programma/apparaat indicator toevoegen
Beleid/niet-beleid indicator toevoegen
Boeken indicator
"""

#functions 
#packages
import pandas as pd
import pyodbc
from rdflib import Graph, URIRef, Namespace


#SQL database connection 
conn = pyodbc.connect(
    r'DRIVER={ODBC Driver 17 for SQL Server};'
    r'SERVER=mfipapp075,50002;'
    r'DATABASE=RHBP;'
    r'Trusted_Connection=yes;'
)
cursor = conn.cursor()


def fProgramOverheadIndicatorDict(x):
    return ProgramOverheadIndicatorDict.get(x,'UnspecifiedProgramOverheadIndicator') 

def fPolicyIndicatorDict(x):
    return PolicyIndicatorDict.get(x,'UnspecifiedPolicy') 

def fTransactionalDirectionIndicatorDict(x):
    return TransactionalDirectionIndicatorDict.get(x,'UnspecifiedTransactionalDirection') 

#dict
ProgramOverheadIndicatorDict = {
        'P': 'Program',
        'A': 'Overhead',
        }

PolicyIndicatorDict = {
        'J': 'Policy',
        'N': 'Non-Policy',
        }

TransactionalDirectionIndicatorDict = {
        'U': 'Outgoing',
        'O': 'Incoming',
        'B': 'Bidirectional',
        'N': 'N.v.t',
        }

# RDF Property
hasProgramOverheadIndicator = 'hasProgramOverheadIndicator'
hasPolicyIndicator = 'hasPolicyIndicator'
hasTransactionalDirectionIndicator = 'hasTransactionalDirectionIndicator'

# Initializing dataframe from file containing the RDF-converted budget structure  
dfbudgetstructureindex = pd.read_csv("Script Input/budgetstructureindex.csv",sep=";",encoding="utf-8",dtype={'BUDGET_STRUCTURE_ELEMENT_ID':int, 'HIERARCHY':str}) 
dfbudgetstructureindex = dfbudgetstructureindex.rename(columns={"BUDGET_STRUCTURE_ELEMENT_ID": "ID"})

# Initializing dataframe containing the IBOS table RIS_BEGROTINGSONDERDELEN
dfibosbegrotingsonderdelen = pd.read_sql("select * from ris_begrotingsonderdelen;", conn, coerce_float=False) 

df_merge = dfbudgetstructureindex.merge(dfibosbegrotingsonderdelen, on = 'ID', how = 'left')


# Define graph, namespace and prefix
g = Graph()
fstr = Namespace("https://www.rijksfinancien.nl/rijksbegrotingsstructuur/")
g.bind(prefix='fstr', namespace=fstr)


# Create a ProgramOverheadIndicator for each element_id
line_count = 0
for row in df_merge.itertuples(index=False):  
    BudgetStructureInstance = df_merge["INSTANCE"][line_count]
    element_id = df_merge["ID"][line_count]
    #print("line_count:", line_count, "- element_id:", element_id, "- BudgetStructureInstance:", BudgetStructureInstance) # makes it easier to follow progress	 
    print(line_count)  
    
    #ProgramOverhead
    ProgramOverheadIndicator = df_merge["PROG_APP_IND"][line_count]
    
    if pd.notna(ProgramOverheadIndicator): 
    
        ProgramOverheadIndicatorInstance = fProgramOverheadIndicatorDict(ProgramOverheadIndicator[0])
        
        #Add ProgramOverheadIndicator to BudgetStructureInstance, 
        #e.g. fstr:BA0010001995-18005 fstr:hasProgramOverheadIndicator fstr:Program .
        subjectRDF = URIRef(fstr[BudgetStructureInstance])
        predicateRDF = URIRef(fstr[hasProgramOverheadIndicator])
        objectRDF = URIRef(fstr[ProgramOverheadIndicatorInstance])    
        triple = (subjectRDF, predicateRDF, objectRDF) 
        
        g.add(triple)
    

    #Policy
    PolicyIndicator = df_merge["BELEID_IND"][line_count]
    
    if pd.notna(PolicyIndicator) : 
        PolicyIndicatorInstance = fPolicyIndicatorDict(PolicyIndicator[0])
        
        #Add PolicyIndicator to BudgetStructureInstance, 
        #e.g. fstr:BA0010001995-18005 fstr:hasPolicyIndicator fstr:Policy .
        subjectRDF = URIRef(fstr[BudgetStructureInstance])
        predicateRDF = URIRef(fstr[hasPolicyIndicator])
        objectRDF = URIRef(fstr[PolicyIndicatorInstance])    
        triple = (subjectRDF, predicateRDF, objectRDF) 
        
        g.add(triple)
        
        
    #TransacionalDirection
    TransactionalDirectionIndicator = df_merge["BOEKEN_JR_T_IND"][line_count]
    
    if pd.notna(TransactionalDirectionIndicator) : 
        TransactionalDirectionIndicatorInstance = fTransactionalDirectionIndicatorDict(TransactionalDirectionIndicator[0])
        
        #Add PolicyIndicator to BudgetStructureInstance, 
        #e.g. fstr:BA0010102003-28431 fstr:hasTransactionalDirectionIndicator fstr:Bidirectional .
        subjectRDF = URIRef(fstr[BudgetStructureInstance])
        predicateRDF = URIRef(fstr[hasTransactionalDirectionIndicator])
        objectRDF = URIRef(fstr[TransactionalDirectionIndicatorInstance])    
        triple = (subjectRDF, predicateRDF, objectRDF) 
        
        g.add(triple)    

    line_count+=1

print("Now serializing graph...")
print(g.serialize(destination='Script Output/outputstructureIndicators.ttl',format='turtle'))
print("Done serializing graph")

conn.close()     #<--- Close the connection
