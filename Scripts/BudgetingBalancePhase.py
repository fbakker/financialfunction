# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a script file to convert CSV-data regarding the budgeting balance phase of the Dutch governmental budget cycle to Turtle script (RDF).

"""

# Importing pandas for manipulating and generating dataframes
import pandas
dfphase = pandas.read_csv("C:/Users/Administrator/Downloads/BudgetPhaseLijst 220321.csv",sep=";",encoding="latin-1",dtype={'BudgetingBalancePhase':str, 'BudgetingBalancePhaseDescription':str, 'BudgetingPhaseTypeNarrow':str, 'BudgetingPhaseTypeBroad':str, 'BudgetCyclePhase':str, 'BudgetingBalanceType':str,'BudgetingBalanceReportingReference':str})

# Define entities with type string for re-usability and adaptability purposes. In addition this may help in making python code in the section where RDF triples are generated more readable.

hasDateTimeDescription ='hasDateTimeDescription'
hasBudgetingBalancePhaseDescription = 'hasBudgetingBalancePhaseDescription'
hasBudgetingBalanceReportingReference = 'hasBudgetingBalanceReportingReference'
hasBudgetingBalanceType = 'hasBudgetingBalanceType'
BudgetingBalancePhase = 'BudgetingBalancePhase'

# Imporing RDFlib for generating or manipulating RDF data.
import rdflib
from rdflib import Graph, URIRef, BNode, Literal, Namespace
from rdflib.namespace import RDF, FOAF, XSD

# Define graph, namespace and prefix
g = Graph()
fcycDEF = Namespace("https://www.rijksfinancien.nl/rijksbegrotingscyclus/def/")
g.bind(prefix='fcyc', namespace=fcycDEF)

fcycID = Namespace("https://www.rijksfinancien.nl/rijksbegrotingscyclus/id/")
g.bind(prefix='fcycID', namespace=fcycID)


print("Done initializing")
print("Now processing rows...")

# Create a budget structure element for each row (prerequisite: dataframe has to be ordered, starting with the higher budget structure elements followed by lower level budget structure elements, i.e. budget chapters before article before article items, et cetera)
line_count = 0

for row in dfphase.itertuples(index=False):
    print(line_count) # makes it easier to follow progress
    PhaseInstance = str(dfphase['BudgetingBalancePhase'][line_count])
    BudgetingBalancePhaseDescriptionInstance = str(dfphase['BudgetingBalancePhaseDescription'][line_count])	
    BudgetingPhaseTypeNarrowInstance = str(dfphase['BudgetingPhaseTypeNarrow'][line_count])
    BudgetingBalanceTypeInstance = str(dfphase['BudgetingBalanceType'][line_count])
    BudgetingBalanceReportingReferenceInstance = str(dfphase['BudgetingBalanceReportingReference'][line_count])

    # BudgetingPhaseInstance is of type BudgetingBalancePhase, e.g. fcyc:phase1-PRE-OW rdf:type fcyc:BudgetingBalancePhase.
    subjectRDF = URIRef(fcycID[PhaseInstance])
    predicateRDF = URIRef(RDF.type)
    objectRDF = URIRef(fcycDEF[BudgetingBalancePhase])
    triple = (subjectRDF, predicateRDF, objectRDF)
    g.add(triple)

    # BudgetingPhaseInstance is of type BudgetingBalancePhase, e.g. fcyc:phase1-PRE-OW rdf:type fcyc:DraftInitialBudgetBill-Phase.
    subjectRDF = URIRef(fcycID[PhaseInstance])
    predicateRDF = URIRef(RDF.type)
    objectRDF = URIRef(fcycDEF[BudgetingPhaseTypeNarrowInstance])
    triple = (subjectRDF, predicateRDF, objectRDF)
    g.add(triple)

    # BudgetingPhaseInstance has a BudgetingBalancePhaseDescription, e.g. fcyc:phase1-PRE-OW fcyc:hasBudgetingBalancePhaseDescription "1. Stand concept ontwerpbegroting: PRE-OW."
    subjectRDF = URIRef(fcycID[PhaseInstance])
    predicateRDF = URIRef(fcycDEF[hasBudgetingBalancePhaseDescription])
    objectRDF = Literal(BudgetingBalancePhaseDescriptionInstance,datatype=XSD.string)
    triple = (subjectRDF, predicateRDF, objectRDF)
    g.add(triple)

    # BudgetingPhaseInstance is of type BudgetingBalancePhase, e.g. fcyc:phase1-PRE-OW fcyc:hasBudgetingBalanceReportingReference fcyc:PreDraftInitialBudgetBill.
    subjectRDF = URIRef(fcycID[PhaseInstance])
    predicateRDF = URIRef(fcycDEF[hasBudgetingBalanceReportingReference])
    objectRDF = URIRef(fcycDEF[BudgetingBalanceReportingReferenceInstance])
    triple = (subjectRDF, predicateRDF, objectRDF)
    g.add(triple)

    # BudgetingPhaseInstance is of type BudgetingBalancePhase, e.g. fcyc:phase1-PRE-OW fcyc:hasBudgetingBalanceType fcyc:CumulativeBalance.
    subjectRDF = URIRef(fcycID[PhaseInstance])
    predicateRDF = URIRef(fcycDEF[hasBudgetingBalanceType])
    objectRDF = URIRef(fcycDEF[BudgetingBalanceTypeInstance])
    triple = (subjectRDF, predicateRDF, objectRDF)
    g.add(triple)
       
    line_count = line_count + 1

print("Done processing rows from budget phase file")
print("Now serializing graph...")
# Writing resulting RDF-graph to file
print (g.serialize(destination='C:/Users/Administrator/Downloads/BudgetingBalancePhase.ttl',format='turtle'))
print("Done serializing graph")
print ("End of program")
