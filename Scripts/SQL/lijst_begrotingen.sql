SELECT bol.JAAR
      ,bol.ID
      ,bol.NR
	  ,bol.OMSCHRIJVING
	  ,bol.ROMEINS_NR
      ,bol.AFKORTING
      ,bol.BGG_TYPE
  FROM RIS_BEGROTINGSONDERDELEN bol
  where bol.BGG_TYPE is not null
  order by bol.JAAR, bol.SORTEER_NR
