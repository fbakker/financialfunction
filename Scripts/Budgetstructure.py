# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a script file to convert CSV-data regarding the budget structure from the IBOS application to Turtle script (RDF).
IBOS being the budgetary administrative system of the Dutch government.

Each row of the source data file contains an element id in the column 'BOL_ID' that signifies a budget structure element to be processed into RDF,
e.g. '185488' being a 'budget article detail item'. Each element ID mentioned in the BOL_ID column is repeated once in one of the columns next to the column 'BOL_ID' to indicate the very
type of that budget structure element, e.g. 'budget chapter', 'budget article', 'budget instrument', et cetera. Depending on the exact column header where
the element ID is repeated, one can deduce the type of budget structure element e.g. "A_ID" column header being a reference to 'budget article'. The columns left
and right to this 'type' column contain references to other element IDs from where we can deduce the higher and lower budget structure hierarchy
elements (parents and children of the current element being processed), e.g. '185482' being a "budget instrument" and the immediate parent of "185488", with '182348' being its parent-parent as a "budget
article item" and "182347" being the parent-parent-parent as a "budget article", "182262" being the "budget chapter" and parent-parent-parent-parent and finally "32" being the top parent referencing "budget year 2020".

Example source data file:

BOL_ID  	JAAR_ID     JAAR_NR     JAAR_OMSCHRIJVING   	BGG_ID  	BGG_NR  	BGG_OMSCHRIJVING	                BTN_ID	        BTN_NR	        BTN_OMSCHRIJVING	    A_ID	    A_NR    	A_OMSCHRIJVING	    AO_ID	    AO_NR   	AO_OMSCHRIJVING	        SAO_ID	        SAO_NR	        SAO_OMSCHRIJVING	INSTRUMENT_ID	INSTRUMENT_NR	INSTRUMENT_OMSCHRIJVING	    DETAIL_ID	DETAIL_NR	DETAIL_OMSCHRIJVING
185488  	32	        2020        2020	                182262  	8	        Onderwijs, Cultuur en Wetenschap		                        		                    182347  	15	        Media       	    182348	    70	        Bekostiging			                                                    	185482      	1           	Bekostiging             	185488  	5	        Nederlands Instituut voor Beeld en Geluid (NIGB)

The BOL_ID is referring in this example to a "budget article detail item".

The difficulty in converting the source data file lies in the fact that the budget structure itself has changed over the years:

1989-2002:       	2003-2019:    	        2020 - 20XX:
Jaar                Jaar                	Jaar
Begroting	        Begroting           	Begroting
Beleidsterrein  	Artikel	                Artikel
Artikel         	Artikelonderdeel    	Artikelonderdeel
Artikelonderdeel	Subartikelonderdeel 	Instrument
Subartikelonderdeel	(Blank)	                Detail

In order for the script to work correctly, the source data has to be ordered as a prerequisite (using the pandas dataframe functionality, this is done in the script automatically).
The data should start with the higher budget structure elements (parents) followed by lower level budget structure elements (children), e.g. budget chapters before budget articles before budget artice items, etcetera.
Only then can the script properly process children elements, as it has to establish the hierachy in which the children are present.

Needed input:

1. An export file from IBOS containing all budget chapters and their attributes, "lijst_begrotingen.csv"
2. An export file from IBOS containing all budget structure elements, "20200623_begrotingsonderdelen_niveau_naam.csv"

Make sure to put these files in the input folder. Then run the script. Running script will result in two generated files:

1. Outputstructure.ttl - a textfile with Turtle extension containing the full budget structure hierarchy in RDF-triples.
2. BudgetStructureHierarchy.csv - a CSV-file containing all the RDF-identifiers for the budget structure parent elements per low-level element_ID (being Sub Article Item or Detail Article Item) and including the RDF-converted element itself

The script takes around 15 minutes for 180.000 rows.

"""
#packages
import pandas as pd # Importing pandas for manipulating and generating dataframes
import pyodbc # Importing pyodbc for connecting to ODBC database

# Imporing RDFlib for generating or manipulating RDF data.
import rdflib
from rdflib import Graph, URIRef, BNode, Literal, Namespace
from rdflib.namespace import RDF, FOAF, XSD


print("Now initializing...")
# Function with dictionary to establish budget chapter type based on code given through argument x
def fBudgetChapterType(x):
    return {
        'B': 'DepartmentBudget',
        'F': 'BudgetFund',
        'O': 'Non-DepartmentBudget',
        'D': 'Dummy',
        'P': 'Contribution',
        'X': 'NotInBudgetSystem',
    }[x]


#SQL database connection 
conn = pyodbc.connect(
    r'DRIVER={ODBC Driver 17 for SQL Server};'
    r'SERVER=mfipapp075,50002;'
    r'DATABASE=RHBP;'
    r'Trusted_Connection=yes;'
)
cursor = conn.cursor()

#Begrotingen
budgetchapter_query = open('SQL/lijst_begrotingen.sql', 'r')
dfbudgetchapter = pd.read_sql(budgetchapter_query.read(), conn, coerce_float=False)

#Begrotingsonderdelen
article_query = open('SQL/begrotingsonderdelen_niveau_naam.sql', 'r')
dfarticle = pd.read_sql(article_query.read(), conn, coerce_float=False)


dfarticle = dfarticle.sort_values(by=["JAAR_NR","BGG_NR", "BTN_NR","A_NR","AO_NR","SAO_NR","INSTRUMENT_NR","DETAIL_NR"], ascending = True, na_position ='first') #this script expects the order of structural elements in the file to be in such a way that first a higher element is defined  (example: "article"), before a lower element can be defined (example: "article item")
dfarticle = dfarticle.reset_index(drop=True) #necessary to reset index after sorting table. This prevents the for-loop (see beneath) to still go by original order of the dataframe and hence run into an error


# Indexes for further lookup
BudgetStructureIndex = {}
AnnualBudgetStructurePeriodIndex = {}
BudgetStructureHierarchyIndex = {}

# Define entities with type string for re-usability and adaptability purposes. In addition this may help in making python code in the section where RDF triples are generated more readable.
# RDF Prefix
# RDF Object
# RDF Property

# RDF Prefix
BudgetStructurePrefix = 'BS'
AnnualBudgetStructurePrefix = 'ABS'
BudgetChapterPrefix= 'BC'
BudgetArticlePrefix= 'BA'
BudgetArticleItemPrefix= 'BAI'
BudgetArticleSubItemPrefix='BASI'
BudgetChapterPolicyAgendaPrefix = 'BCPA'
BudgetPolicyDomainPrefix = 'BPD'
BudgetInstrumentPrefix = 'BI'
BudgetArticleDetailItemPrefix = 'BADI'
NationalBudgetPeriodPrefix = 'BP'
NationalBudgetFactPeriodPrefix = 'BFP'
BudgetMemorandumPeriodPrefix = 'BM'
ActiveBudgetStructurePeriodPrefix = 'ABSP'
DateTimeDescriptionPrefix = 'DTD'
DateBeginPrefix = 'DATEBEGIN'
DateEndPrefix ='DATEEND'

# RDF Object
UnitYear = 'unitYear'
AnnualBudgetStructure = 'AnnualBudgetStructure'
BudgetChapter = 'BudgetChapter'
BudgetPolicyDomain = 'BudgetPolicyDomain'
BudgetArticle = 'BudgetArticle'
BudgetArticleItem = 'BudgetArticleItem'
BudgetArticleSubItem = 'BudgetArticleSubItem'
BudgetArticleDetailItem = 'BudgetArticleDetailItem'
BudgetInstrument = 'BudgetInstrument'

# RDF Property
hasDateTimeDescription ='hasDateTimeDescription'
rdfType = 'rdf:type'
hasBeginning = 'hasBeginning'
hasEnd = 'hasEnd'
inXSDDate = 'inXSDDate'
unitType = 'unitType'
Year = 'year'
hasAnnualBudgetStructure = 'hasAnnualBudgetStructure'
hasActiveBudgetStructurePeriod = 'hasActiveBudgetStructurePeriod'
hasBudgetChapter = 'hasBudgetChapter'
hasBudgetChapterType = 'hasBudgetChapterType'
hasBudgetChapterName = 'hasBudgetChapterName'
hasBudgetChapterNumber = 'hasBudgetChapterNumber'
hasBudgetChapterRomanNumber = 'hasBudgetChapterRomanNumber'
hasBudgetChapterAbbreviation = 'hasBudgetChapterAbbreviation'
hasBudgetChapterPolicyAgenda = 'hasBudgetChapterPolicyAgenda'
hasBudgetStructureChildElement = 'hasBudgetStructureChildElement'
hasBudgetStructureParentElement = 'hasBudgetStructureParentElement'
hasBudgetStructureInstanceName = 'hasBudgetStructureInstanceName'
hasBudgetStructureElementName = 'hasBudgetStructureElementName'
hasBudgetStructureElementNumber = 'hasBudgetStructureElementNumber'
hasBudgetStructureIdentifier = 'hasBudgetStructureIdentifier'


# Define graph, namespace and prefix
g = Graph()
fstr = Namespace("https://www.rijksfinancien.nl/rijksbegrotingsstructuur/")
g.bind(prefix='fstr', namespace=fstr)
time = Namespace("http://www.w3.org/2006/time#/")
g.bind(prefix='time', namespace=time)

# Initializing budget structure indexes by adding headers to dictionary. These will be used during the execution of the script when referring previously processed elements of the budget structure, as those can have relations with the current element being processed.
BudgetStructureIndex.update({'BUDGET_STRUCTURE_ELEMENT_ID': 'INSTANCE'}) #Creating an index to lookup an already processed budget structure element instance so that we may retrieve the converted RDF-entity belonging to that element
BudgetStructureHierarchyIndex.update({'BUDGET_STRUCTURE_ELEMENT_ID': 'HIERARCHY'}) #Creating an index to lookup the budget structure hierarchy of an already processed budget structure element instance. Budget structures used within the Dutch government contain hierachies that can change through the years and give more detailed meaning to what a budgetary transaction or budgetary balance statement entails.

# Create budget periods - range includes 1989 up and till 2040
for year in range(1989, 2041):

        # Budget Memorandum Period
        
        # DatetimeDescription for each budget memorandum year, e.g. fstr:BM1989 time:hasDateTimeDescription fstr:DTD1989
        BudgetMemorandumPeriod = BudgetMemorandumPeriodPrefix + str(year)
        DateTimeDescription = DateTimeDescriptionPrefix+str(year)
        subjectRDF =  URIRef(fstr[BudgetMemorandumPeriod])
        predicateRDF = URIRef(time[hasDateTimeDescription])
        objectRDF = URIRef(fstr[DateTimeDescription])
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)

        # Beginning for each budget memorandum year, e.g. fstr:BM1989 time:hasBeginning fstr:DATEBEGIN1989
        DateTimeDescription = DateTimeDescriptionPrefix+str(year)
        DateBegin = DateBeginPrefix + str(year)
        subjectRDF =  URIRef(fstr[BudgetMemorandumPeriod])
        predicateRDF = URIRef(time[hasBeginning])
        objectRDF = URIRef(fstr[DateBegin])
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)

        # Ending for each budget memorandum year, e.g. fstr:BM1989 time:hasEnd fstr:DATEEND1989
        DateTimeDescription = DateTimeDescriptionPrefix+str(year)
        DateEnd = DateEndPrefix + str(year)
        subjectRDF =  URIRef(fstr[BudgetMemorandumPeriod])
        predicateRDF = URIRef(time[hasEnd])
        objectRDF = URIRef(fstr[DateEnd])
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)
        

        # National Budget Period
        
        # DatetimeDescription for each national budget year, e.g. fstr:BP1989 time:hasDateTimeDescription fstr:DTD1989 ;
        NationalBudgetPeriod = NationalBudgetPeriodPrefix + str(year)
        subjectRDF =  URIRef(fstr[NationalBudgetPeriod])
        predicateRDF = URIRef(time[hasDateTimeDescription])
        objectRDF = URIRef(fstr[DateTimeDescription])
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)

        # Beginning for each national budget year, e.g. fstr:BP1989 time:hasBeginning fstr:DATEBEGIN1989
        DateTimeDescription = DateTimeDescriptionPrefix+str(year)
        DateBegin = DateBeginPrefix + str(year)
        subjectRDF =  URIRef(fstr[NationalBudgetPeriod])
        predicateRDF = URIRef(time[hasBeginning])
        objectRDF = URIRef(fstr[DateBegin])
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)

        # Ending for each national budget year, e.g. fstr:BP1989 time:hasEnd fstr:DATEEND1989 .
        DateTimeDescription = DateTimeDescriptionPrefix+str(year)
        DateEnd = DateEndPrefix + str(year)
        subjectRDF =  URIRef(fstr[NationalBudgetPeriod])
        predicateRDF = URIRef(time[hasEnd])
        objectRDF = URIRef(fstr[DateEnd])
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)


        # National Budget Fact Period

        # DatetimeDescription for each national budget fact year, e.g. fstr:BPF1989 time:hasDateTimeDescription fstr:DTD1989 ;
        NationalBudgetFactPeriod = NationalBudgetFactPeriodPrefix + str(year)
        subjectRDF =  URIRef(fstr[NationalBudgetFactPeriod])
        predicateRDF = URIRef(time[hasDateTimeDescription])
        objectRDF = URIRef(fstr[DateTimeDescription])
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)

        # Beginning for each national budget fact year, e.g. fstr:BPF1989 time:hasBeginning fstr:DATEBEGIN1989
        DateTimeDescription = DateTimeDescriptionPrefix+str(year)
        DateBegin = DateBeginPrefix + str(year)
        subjectRDF =  URIRef(fstr[NationalBudgetFactPeriod])
        predicateRDF = URIRef(time[hasBeginning])
        objectRDF = URIRef(fstr[DateBegin])
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)

        # Ending for each national budget fact year, e.g. fstr:BPF1989 time:hasEnd fstr:DATEEND1989 .
        DateTimeDescription = DateTimeDescriptionPrefix+str(year)
        DateEnd = DateEndPrefix + str(year)
        subjectRDF =  URIRef(fstr[NationalBudgetFactPeriod])
        predicateRDF = URIRef(time[hasEnd])
        objectRDF = URIRef(fstr[DateEnd])
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)

     

        # Generic Date/Time triples
        
        # DateBegin has value, e.g. fstr:DATEBEGIN1989 time:inXSDDate "1989-01-01"^^xsd:date .
        DateBegin = DateBeginPrefix + str(year)
        subjectRDF =  URIRef(fstr[DateBegin])
        predicateRDF = URIRef(time[inXSDDate])
        objectRDF = Literal(str(year)+'-01-01',datatype=XSD.date)
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)

        # DateEnd has value, e.g. fstr:DATEEND1989 time:inXSDDate "1989-12-31"^^xsd:date .
        DateEnd = DateEndPrefix + str(year)
        subjectRDF =  URIRef(fstr[DateEnd])
        predicateRDF = URIRef(time[inXSDDate])
        objectRDF = Literal(str(year)+'-12-31',datatype=XSD.date)
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)

        # DateTimeDescription has unitType year, e.g. fstr:DTD1989 time:unitType time:unitYear ;
        DateTimeDescription = DateTimeDescriptionPrefix+str(year)
        subjectRDF =  URIRef(fstr[DateTimeDescription])
        predicateRDF = URIRef(time[unitType])
        objectRDF = URIRef(time[UnitYear])
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)

        # DateTimeDescription has year value, e.g. fstr:DTD1989 time:year "1989"^^xsd:gYear .
        DateTimeDescription = DateTimeDescriptionPrefix+str(year)
        subjectRDF =  URIRef(fstr[DateTimeDescription])
        predicateRDF = URIRef(time.year) # has to be through the attribute (.) of time and not through the dict [] of time as year is used already as an local variable.
        objectRDF = Literal(str(year),datatype=XSD.gYear, normalize=False) #has not to be normalized due to bug in RDFlib leading to undesired date format XXXX-YY-ZZ.
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)
              

# Create a National Budgetstructure. In the future parallel budget structures could be serviced through the creation of other instances (e.g. an instance for a budget structure of the European Union) next to the current national budget structure. E.g. fstr:BS00001 rdf:type fstr:BudgetStructure ;
BudgetStructureInstance = BudgetStructurePrefix+'00001' #This is the Dutch National Budget Structure
BudgetStructure = 'BudgetStructure'
subjectRDF =  URIRef(fstr[BudgetStructureInstance])
predicateRDF = URIRef(RDF.type)
objectRDF = URIRef(fstr[BudgetStructure])
triple = (subjectRDF, predicateRDF, objectRDF)
g.add(triple)

# Give name to the National Budgetstructure - English, e.g. fstr:BS00001 fstr:hasBudgetStructureInstanceName "Dutch National Budget Structure"@en
BudgetStructureInstanceName = 'Dutch National Budget Structure'
subjectRDF =  URIRef(fstr[BudgetStructureInstance])
predicateRDF = URIRef(fstr[hasBudgetStructureElementName])
objectRDF = Literal(BudgetStructureInstanceName,lang='EN')
triple = (subjectRDF, predicateRDF, objectRDF)
g.add(triple)

# Give name to the National Budgetstructure - Dutch, e.g. fstr:BS00001 fstr:hasBudgetStructureInstanceName "Nederlandse Rijksbegrotingsstructuur"@nl
BudgetStructureInstanceName = 'Nederlandse Rijksbegrotingsstructuur'
subjectRDF =  URIRef(fstr[BudgetStructureInstance])
predicateRDF = URIRef(fstr[hasBudgetStructureElementName])
objectRDF = Literal(BudgetStructureInstanceName,lang='NL')
triple = (subjectRDF, predicateRDF, objectRDF)
g.add(triple)

print("Done initializing")
print("Now processing rows...")

# Create a budget structure element for each row (prerequisite: dataframe has to be ordered, starting with the higher budget structure elements followed by lower level budget structure elements, i.e. budget chapters before article before article items, et cetera)
line_count = 0
for row in dfarticle.itertuples(index=False):
    print(line_count) # makes it easier to follow progress
    year = str(dfarticle['JAAR_NR'][line_count])
    ElementID = int(dfarticle['BOL_ID'][line_count])
    dfarticle2 = dfarticle.loc[line_count] [['JAAR_ID', 'BGG_ID', 'BTN_ID', 'A_ID', 'AO_ID', 'SAO_ID', 'INSTRUMENT_ID','DETAIL_ID']].copy() #needed to create an additional dataframe to establish what entity is defined in the CSV source data file, as it isn't known upfront which other column in the CSV-file describes the element mentioned in column 'BOL_ID'. Hence we need to iterate through each possible column per row, knowing only the table identifier 'BOL_ID' and establish what column is containing that very same ID.
    ElementTypeIndicator = dfarticle2[dfarticle2==ElementID].index.values[0] #Used to establish the type of the element to be processed

    if ElementTypeIndicator == 'JAAR_ID': #Each budget year has one budget structure. Appearantly the element identified is the ID of the budget structure of a specific budget year

        # Creating an annual budget structure instance
        AnnualBudgetStructureInstance = AnnualBudgetStructurePrefix + str(year)
        BudgetStructureIndex.update({ElementID: AnnualBudgetStructureInstance}) #Creating an index to lookup an already processed budget structure element instance for a year

        # AnnualBudgetStructureInstance is of type AnnualBudgetStructur, e.g. fstr:ABS1989 rdf:type fstr:AnnualBudgetStructure
        subjectRDF = URIRef(fstr[AnnualBudgetStructureInstance])
        predicateRDF = URIRef(RDF.type)
        objectRDF = URIRef(fstr[AnnualBudgetStructure])
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)

        # Budgetstructure has AnnualBudgetStructureInstance, e.g. fstr:BS00001 fstr:hasAnnualBudgetStructure fstr:ABS1989
        subjectRDF =  URIRef(fstr[BudgetStructureInstance])
        predicateRDF = URIRef(fstr[hasAnnualBudgetStructure])
        objectRDF = URIRef(fstr[AnnualBudgetStructureInstance])
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)

        # AnnualBudgetStructureInstance has ActiveBudgetStructurePeriod, e.g. fstr:ABS1989 fstr:hasActiveBudgetStructurePeriod fstr:ABSP1989
        AnnualBudgetStructureInstance = AnnualBudgetStructurePrefix + str(year)
        ActiveBudgetStructurePeriod = ActiveBudgetStructurePeriodPrefix + str(year)
        AnnualBudgetStructurePeriodIndex.update({str(year):ActiveBudgetStructurePeriod})
        subjectRDF =  URIRef(fstr[AnnualBudgetStructureInstance])
        predicateRDF = URIRef(fstr[hasActiveBudgetStructurePeriod])
        objectRDF = URIRef(fstr[ActiveBudgetStructurePeriod])
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)

        # ActiveBudgetStructurePeriod has DatetimeDescription, e.g. fstr:ABSP1989 time:hasDateTimeDescription fstr:DTD1989
        DateTimeDescription = DateTimeDescriptionPrefix+str(year)
        subjectRDF =  URIRef(fstr[ActiveBudgetStructurePeriod])
        predicateRDF = URIRef(time[hasDateTimeDescription])
        objectRDF = URIRef(fstr[DateTimeDescription])
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)

        # ActiveBudgetStructurePeriod has Beginning, e.g. fstr:ABSP1989 time:hasBeginning fstr:DATEBEGIN1989
        DateTimeDescription = DateTimeDescriptionPrefix+str(year)
        DateBegin = DateBeginPrefix + str(year)
        subjectRDF =  URIRef(fstr[ActiveBudgetStructurePeriod])
        predicateRDF = URIRef(time[hasBeginning])
        objectRDF = URIRef(fstr[DateBegin])
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)

        # ActiveBudgetStructurePeriod has Ending, e.g. fstr:ABSP1989 time:hasEnd fstr:DATEEND1989
        DateTimeDescription = DateTimeDescriptionPrefix+str(year)
        DateEnd = DateEndPrefix + str(year)
        subjectRDF =  URIRef(fstr[ActiveBudgetStructurePeriod])
        predicateRDF = URIRef(time[hasEnd])
        objectRDF = URIRef(fstr[DateEnd])
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)
        
        # Add BudgetStructureIdentifier (bol_id from IBOS), e.g. fstr:ABSP1989-21 fstr:hasBudgetStructureIdentifier "21"^^xsd:integer ;
        subjectRDF = URIRef(fstr[AnnualBudgetStructureInstance])
        predicateRDF = URIRef(fstr[hasBudgetStructureIdentifier])
        BudgetStructureIdentifier = ElementID
        objectRDF = Literal(BudgetStructureIdentifier,datatype=XSD.integer)
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)

    elif ElementTypeIndicator == 'BGG_ID': # Ministries within the Dutch government have one or more budget chapters to administrate their budgets. Each budget chapter contains a hierarchy of lower budget structure elements.

        # Create BudgetChapterIdentifier
        BudgetChapterInstanceNumber = str(dfbudgetchapter.loc[dfbudgetchapter['ID']==ElementID]['NR'].values[0])
        BudgetChapterInstance = BudgetChapterPrefix + BudgetChapterInstanceNumber + '-' + str(year) + '-' + str(ElementID)
        BudgetStructureIndex.update({ElementID: BudgetChapterInstance}) #Adding newly created RDF element to index

        # BudgetChapterInstance is of type BudgetChapter, e.g. fstr:BC000101999-66 rdf:type fstr:BudgetChapter
        subjectRDF = URIRef(fstr[BudgetChapterInstance])
        predicateRDF = URIRef(RDF.type)
        objectRDF = URIRef(fstr[BudgetChapter])
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)

        # Add BudgetChapter as child value to AnnualBudgetStructureInstance, e.g. fstr:ABSP1999 fstr:hasBudgetStructureChildElement fstr:BC000101999-66
        subjectRDF = URIRef(fstr[AnnualBudgetStructureInstance])
        predicateRDF = URIRef(fstr[hasBudgetStructureChildElement])
        objectRDF = URIRef(fstr[BudgetChapterInstance])
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)

        # Add AnnualBudgetStructureInstance as parent value to BudgetChapter, e.g. fstr:BC000101999-66 fstr:hasBudgetStructureParentElement fstr:ABS1999
        subjectRDF = URIRef(fstr[BudgetChapterInstance])
        predicateRDF = URIRef(fstr[hasBudgetStructureParentElement])
        objectRDF = URIRef(fstr[AnnualBudgetStructureInstance])
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)

        # Add Budget Chapter type to budgetchapter, e.g. fstr:BC000101999-66 fstr:hasBudgetChapterType fstr:DepartmentBudget
        subjectRDF = URIRef(fstr[BudgetChapterInstance])
        predicateRDF = URIRef(fstr[hasBudgetChapterType])
        BudgetChapterType = fBudgetChapterType(dfbudgetchapter.loc[dfbudgetchapter['ID']==ElementID]['BGG_TYPE'].values[0])
        objectRDF = URIRef(fstr[BudgetChapterType])
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)

        # Add AnnualBudgetStructurePeriod to BudgetChapter, e.g. fstr:BC000101999-66 fstr:hasActiveBudgetStructurePeriod fstr:ABSP1999
        subjectRDF = URIRef(fstr[BudgetChapterInstance])
        predicateRDF = URIRef(fstr[hasActiveBudgetStructurePeriod])
        AnnualBudgetStructurePeriodInstance = AnnualBudgetStructurePeriodIndex[year]
        objectRDF = URIRef(fstr[AnnualBudgetStructurePeriodInstance])
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)

        # Add BudgetChapterName, e.g. fstr:BC000101999-66 hasBudgetStructureElementName "Defensie"^^xsd:string ;
        subjectRDF = URIRef(fstr[BudgetChapterInstance])
        predicateRDF = URIRef(fstr[hasBudgetStructureElementName])
        BudgetChapterInstanceName = str(dfbudgetchapter.loc[dfbudgetchapter['ID']==ElementID]['OMSCHRIJVING'].values[0])
        objectRDF = Literal(BudgetChapterInstanceName,datatype=XSD.string)
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)

        # Add BudgetChapterNumber, e.g. fstr:BC000101999-66 hasBudgetStructureElementNumber "10"^^xsd:string ;
        subjectRDF = URIRef(fstr[BudgetChapterInstance])
        predicateRDF = URIRef(fstr[hasBudgetStructureElementNumber])
        BudgetChapterInstanceNumber = str(dfbudgetchapter.loc[dfbudgetchapter['ID']==ElementID]['NR'].values[0])
        objectRDF = Literal(BudgetChapterInstanceNumber,datatype=XSD.string)
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)
        
        # Add BudgetStructureIdentifier (bol_id from IBOS), e.g. fstr:BC000101999-66 fstr:hasBudgetStructureIdentifier "66"^^xsd:integer ;
        subjectRDF = URIRef(fstr[BudgetChapterInstance])
        predicateRDF = URIRef(fstr[hasBudgetStructureIdentifier])
        BudgetStructureIdentifier = ElementID
        objectRDF = Literal(BudgetStructureIdentifier,datatype=XSD.integer)
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)

        # Add BudgetChapterRomanNumber, e.g. fstr:BC000101999-66 fstr:hasBudgetChapterRomanNumber "X"^^xsd:string ;
        subjectRDF = URIRef(fstr[BudgetChapterInstance])
        predicateRDF = URIRef(fstr[hasBudgetChapterRomanNumber])
        BudgetChapterInstanceRomanNumber = str(dfbudgetchapter.loc[dfbudgetchapter['ID']==ElementID]['ROMEINS_NR'].values[0])
        objectRDF = Literal(BudgetChapterInstanceRomanNumber,datatype=XSD.string)
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)

        # Add BudgetChapter Abbreviation, e.g. fstr:BC000101999-66 fstr:hasBudgetChapterAbbreviation "DEF"^^xsd:string ;
        subjectRDF = URIRef(fstr[BudgetChapterInstance])
        predicateRDF = URIRef(fstr[hasBudgetChapterAbbreviation])
        BudgetChapterInstanceAbbreviation = str(dfbudgetchapter.loc[dfbudgetchapter['ID']==ElementID]['AFKORTING'].values[0])
        objectRDF = Literal(BudgetChapterInstanceAbbreviation,datatype=XSD.string)
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)

        # Add BudgetChapter PolicyAgenda, e.g. fstr:BC000101999-66 fstr:hasBudgetChapterPolicyAgenda fstr:BCPA000101999
        BudgetChapterPolicyAgenda = BudgetChapterPolicyAgendaPrefix + '-' + BudgetChapterPrefix + BudgetChapterInstanceNumber + '-' + str(year)
        subjectRDF = URIRef(fstr[BudgetChapterInstance])
        predicateRDF = URIRef(fstr[hasBudgetChapterPolicyAgenda])
        objectRDF = URIRef(fstr[BudgetChapterPolicyAgenda])
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)

    elif ElementTypeIndicator == 'BTN_ID': # In some historic budget years the budget chapters contain a budget policy domain to indicate the budgetary theme being budgeted

        # Establish budget chapter parent and its value. It is assumed that a budget policy domain always has a budget chapter. Throughout history this has proven to be true. In the future this logic might break down.
        BudgetChapterID = int(dfarticle2['BGG_ID'])
        BudgetChapterInstance = BudgetStructureIndex[BudgetChapterID] #Get RDF identifier for budgetchapter from index
        BudgetChapterInstanceNumber = str(dfbudgetchapter.loc[dfbudgetchapter['ID']==BudgetChapterID]['NR'].values[0])

        # Create PolicyDomain Identifier
        BudgetPolicyDomainInstanceNumber = str(int(dfarticle['BTN_NR'][line_count]))
        BudgetPolicyDomainInstanceName = str(dfarticle['BTN_OMSCHRIJVING'][line_count])
        BudgetPolicyDomainInstance = BudgetChapterPrefix + BudgetChapterInstanceNumber + BudgetPolicyDomainPrefix + BudgetPolicyDomainInstanceNumber + "-" + str(year) + '-' + str(ElementID)
        BudgetStructureIndex.update({ElementID: BudgetPolicyDomainInstance}) #Adding newly created RDF element to index

        # PolicyDomainInstance is of type PolicyDomain, e.g. fstr:BPD001001999-2841 rdf:type fstr:BudgetPolicyDomain
        subjectRDF = URIRef(fstr[BudgetPolicyDomainInstance])
        predicateRDF = URIRef(RDF.type)
        objectRDF = URIRef(fstr[BudgetPolicyDomain])
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)

        # BudgetChapterInstance has child value PolicyDomainInstance, e.g. fstr:BC000101999-66 fstr:hasBudgetStructureChildElement fstr:BPD001001999-2841
        subjectRDF = URIRef(fstr[BudgetChapterInstance])
        predicateRDF = URIRef(fstr[hasBudgetStructureChildElement])
        objectRDF = URIRef(fstr[BudgetPolicyDomainInstance])
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)

        # PolicyDomainInstance has parent value BudgetChapterInstance, e.g. fstr:BPD001001999-2841 fstr:hasBudgetStructureParentElement fstr:BC000101999-66
        subjectRDF = URIRef(fstr[BudgetPolicyDomainInstance])
        predicateRDF = URIRef(fstr[hasBudgetStructureParentElement])
        objectRDF = URIRef(fstr[BudgetChapterInstance])
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)

        # Add name to policy domain, e.g. fstr:BPD001001999-2841 fstr:hasBudgetStructureElementName "Overige"^^xsd:string
        subjectRDF = URIRef(fstr[BudgetPolicyDomainInstance])
        predicateRDF = URIRef(fstr[hasBudgetStructureElementName])
        objectRDF = Literal(BudgetPolicyDomainInstanceName,datatype=XSD.string)
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)

        # Add number to policy domain, e.g. fstr:BPD001001999-2841 fstr:BudgetStructureElementNumber "0"^^xsd:string
        subjectRDF = URIRef(fstr[BudgetPolicyDomainInstance])
        predicateRDF = URIRef(fstr[hasBudgetStructureElementNumber])
        objectRDF = Literal(BudgetPolicyDomainInstanceNumber,datatype=XSD.string)
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)
        
        # Add BudgetStructureIdentifier (bol_id from IBOS), e.g. fstr:BPD001001999-2841 fstr:hasBudgetStructureIdentifier "2841"^^xsd:integer ;
        subjectRDF = URIRef(fstr[BudgetPolicyDomainInstance])
        predicateRDF = URIRef(fstr[hasBudgetStructureIdentifier])
        BudgetStructureIdentifier = ElementID
        objectRDF = Literal(BudgetStructureIdentifier,datatype=XSD.integer)
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)

    elif ElementTypeIndicator == 'A_ID': # A consistent element throughout the history of the Dutch National Budget is the budget article which could be said to be the main focus, as the budgetary mandate of the National States-General (Senate and House of Repesentatives) is based on mandating amounts on budget article level.

        # Establish budget chapter parent and its value. It is assumed that an article always has a budget chapter. Throughout history this has proven to be true. In the future this logic might break down.
        BudgetChapterID = int(dfarticle2['BGG_ID'])
        BudgetChapterInstance = BudgetStructureIndex[BudgetChapterID] #Get RDF identifier for budgetchapter from index
        BudgetChapterInstanceNumber = str(dfbudgetchapter.loc[dfbudgetchapter['ID']==BudgetChapterID]['NR'].values[0])

        # Establish budget policy domain parent and its value (if there is a budget policy domain)
        if pd.notna(dfarticle2['BTN_ID']):
            BudgetPolicyDomainID = int(dfarticle2['BTN_ID'])
            BudgetPolicyDomainInstance = BudgetStructureIndex[BudgetPolicyDomainID] #Get RDF identifier for budgetchapter from index
            BudgetPolicyDomainInstanceNumber = str(int(dfarticle['BTN_NR'][line_count]))

            # Create Article Identifier
            BudgetArticleInstanceNumber = str(int(dfarticle['A_NR'][line_count]))
            BudgetArticleInstanceName = str(dfarticle['A_OMSCHRIJVING'][line_count])
            BudgetArticleInstance = BudgetChapterPrefix + BudgetChapterInstanceNumber + BudgetPolicyDomainPrefix + BudgetPolicyDomainInstanceNumber + BudgetArticlePrefix + BudgetArticleInstanceNumber + '-' + str(year) + '-' + str(ElementID)
            BudgetStructureIndex.update({ElementID: BudgetArticleInstance}) #Adding newly created RDF element to index

            # BudgetArticleInstance is of type BudgetArticle, e.g. fstr:BA0010001999-8960 rdf:type fstr:BudgetArticle
            subjectRDF = URIRef(fstr[BudgetArticleInstance])
            predicateRDF = URIRef(RDF.type)
            objectRDF = URIRef(fstr[BudgetArticle])
            triple = (subjectRDF, predicateRDF, objectRDF)
            g.add(triple)

            # PolicyDomainInstance has child value BudgetArticleInstance, e.g. fstr:BPD001001999-2841 fstr:hasBudgetStructureChildElement fstr:BA0010001999-8960
            subjectRDF = URIRef(fstr[BudgetPolicyDomainInstance])
            predicateRDF = URIRef(fstr[hasBudgetStructureChildElement])
            objectRDF = URIRef(fstr[BudgetArticleInstance])
            triple = (subjectRDF, predicateRDF, objectRDF)
            g.add(triple)

            # BudgetArticleInstance has parent value PolicyDomainInstance, e.g. fstr:BA0010001999-8960 fstr:hasBudgetStructureParentElement fstr:BPD001001999-2841
            subjectRDF = URIRef(fstr[BudgetArticleInstance])
            predicateRDF = URIRef(fstr[hasBudgetStructureParentElement])
            objectRDF = URIRef(fstr[BudgetPolicyDomainInstance])
            triple = (subjectRDF, predicateRDF, objectRDF)
            g.add(triple)

        else: #it is assumed that, if there is no budget policy domain instance defined for this row, the budget article is the direct child of the budget chapter. Throughout history this has proven to be true. However, in the future the budget hierachy structure may be reordered and this logic might break down. Until then we keep this logic for complexity/performance reasons.

            # Create Article Identifier
            BudgetArticleInstanceNumber = str(int(dfarticle['A_NR'][line_count]))
            BudgetArticleInstanceName = str(dfarticle['A_OMSCHRIJVING'][line_count])
            BudgetArticleInstance = BudgetChapterPrefix + BudgetChapterInstanceNumber + BudgetArticlePrefix + BudgetArticleInstanceNumber + "-" + str(year) + '-' + str(ElementID)
            BudgetStructureIndex.update({ElementID: BudgetArticleInstance}) #Adding newly created RDF element to index

            # BudgetArticleInstance is of type BudgetArticle, e.g. fstr:BA00522020-183256 rdf:type fstr:BudgetArticle
            subjectRDF = URIRef(fstr[BudgetArticleInstance])
            predicateRDF = URIRef(RDF.type)
            objectRDF = URIRef(fstr[BudgetArticle])
            triple = (subjectRDF, predicateRDF, objectRDF)
            g.add(triple)

            # BudgetChapterInstance has child value BudgetArticleInstance, e.g. fstr:BC00052020-183115 hasBudgetStructureChildElement fstr:BA00522020-183256
            subjectRDF = URIRef(fstr[BudgetChapterInstance])
            predicateRDF = URIRef(fstr[hasBudgetStructureChildElement])
            objectRDF = URIRef(fstr[BudgetArticleInstance])
            triple = (subjectRDF, predicateRDF, objectRDF)
            g.add(triple)

            # BudgetArticleInstance has parent value BudgetChapterInstance, e.g. fstr:BA00522020-183256 fstr:hasBudgetStructureParentElement fstr:BC00052020-183115
            subjectRDF = URIRef(fstr[BudgetArticleInstance])
            predicateRDF = URIRef(fstr[hasBudgetStructureParentElement])
            objectRDF = URIRef(fstr[BudgetChapterInstance])
            triple = (subjectRDF, predicateRDF, objectRDF)
            g.add(triple)

        # Add name to article, e.g. fstr:BA00522020-183256 fstr:hasBudgetStructureElementName "Veiligheid en stabiliteit"^^xsd:string
        subjectRDF = URIRef(fstr[BudgetArticleInstance])
        predicateRDF = URIRef(fstr[hasBudgetStructureElementName])
        objectRDF = Literal(BudgetArticleInstanceName,datatype=XSD.string)
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)

        # Add number to article, e.g. fstr:BA00522020-183256 fstr:hasBudgetStructureElementNumber "2"^^xsd:string
        subjectRDF = URIRef(fstr[BudgetArticleInstance])
        predicateRDF = URIRef(fstr[hasBudgetStructureElementNumber])
        objectRDF = Literal(BudgetArticleInstanceNumber,datatype=XSD.string)
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)
        
        # Add BudgetStructureIdentifier (bol_id from IBOS system), e.g. fstr:BA00522020-183256 fstr:hasBudgetStructureIdentifier "183256"^^xsd:integer ;
        subjectRDF = URIRef(fstr[BudgetArticleInstance])
        predicateRDF = URIRef(fstr[hasBudgetStructureIdentifier])
        BudgetStructureIdentifier = ElementID
        objectRDF = Literal(BudgetStructureIdentifier,datatype=XSD.integer)
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)

    elif ElementTypeIndicator == 'AO_ID': #A refinement of a budget article, the budget article item brings more detail information on what is being budgeted.

        # Establish budget chapter parent and its value. It is assumed that an article item always has a budget chapter. Throughout history this has proven to be true. In the future this logic might break down.
        BudgetChapterID = int(dfarticle2['BGG_ID'])
        BudgetChapterInstanceNumber = str(dfbudgetchapter.loc[dfbudgetchapter['ID']==BudgetChapterID]['NR'].values[0])

        # Establish budget policy domain parent and its value (if there is a budget policy domain)
        if pd.notna(dfarticle2['BTN_ID']):
            BudgetPolicyDomainID = int(dfarticle2['BTN_ID'])
            BudgetPolicyDomainInstanceNumber = str(int(dfarticle['BTN_NR'][line_count]))
        else:
            BudgetPolicyDomainInstanceNumber=''

        # Establish article parent and its value. It is assumed that an article item always has an article. Throughout history this has proven to be true. In the future this logic might break down.
        BudgetArticleID = int(dfarticle2['A_ID'])
        BudgetArticleInstanceNumber = str(int(dfarticle['A_NR'][line_count]))
        BudgetArticleInstance = BudgetStructureIndex[BudgetArticleID] #Get RDF identifier for budget article from index

        # Create Article Item Identifier
        BudgetArticleItemInstanceNumber = str(int(dfarticle['AO_NR'][line_count]))
        BudgetArticleItemInstanceName = str(dfarticle['AO_OMSCHRIJVING'][line_count])
        BudgetArticleItemInstance = BudgetChapterPrefix + BudgetChapterInstanceNumber + BudgetPolicyDomainPrefix + BudgetPolicyDomainInstanceNumber + BudgetArticlePrefix + BudgetArticleInstanceNumber + BudgetArticleItemPrefix + BudgetArticleItemInstanceNumber + "-" + str(year) + '-' + str(ElementID)
        BudgetStructureIndex.update({ElementID: BudgetArticleItemInstance}) #Adding newly created RDF element to index

        # BudgetArticleItemInstance is of type BudgetArticleItem, e.g. fstr:BAI052102020-183262 rdf:type fstr:BudgetArticleItem
        subjectRDF = URIRef(fstr[BudgetArticleItemInstance])
        predicateRDF = URIRef(RDF.type)
        objectRDF = URIRef(fstr[BudgetArticleItem])
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)

        # BudgetArticleInstance has child value BudgetArticleItemInstance, e.g. fstr:BA00522020-183256 fstr:hasBudgetStructureChildElement fstr:BAI052102020-183262
        subjectRDF = URIRef(fstr[BudgetArticleInstance])
        predicateRDF = URIRef(fstr[hasBudgetStructureChildElement])
        objectRDF = URIRef(fstr[BudgetArticleItemInstance])
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)

        # BudgetArticleItemInstance has parent value BudgetArticleInstance, e.g. fstr:BAI052102020-183262 fstr:hasBudgetStructureParentElement fstr:BA00522020-183256
        subjectRDF = URIRef(fstr[BudgetArticleItemInstance])
        predicateRDF = URIRef(fstr[hasBudgetStructureParentElement])
        objectRDF = URIRef(fstr[BudgetArticleInstance])
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)

        # Add name to article item, e.g. fstr:BAI052102020-183262 fstr:hasBudgetStructureElementName "Doorberekening Defensie diversen"^^xsd:string
        subjectRDF = URIRef(fstr[BudgetArticleItemInstance])
        predicateRDF = URIRef(fstr[hasBudgetStructureElementName])
        objectRDF = Literal(BudgetArticleItemInstanceName,datatype=XSD.string)
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)

        # Add number to article item, e.g. fstr:BAI052102020-183262 hasBudgetStructureElementNumber "10"^^xsd:string
        subjectRDF = URIRef(fstr[BudgetArticleItemInstance])
        predicateRDF = URIRef(fstr[hasBudgetStructureElementNumber])
        objectRDF = Literal(BudgetArticleItemInstanceNumber,datatype=XSD.string)
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)
        
        # Add BudgetStructureIdentifier (bol_id from IBOS system), e.g. fstr:BAI052102020-183262 fstr:hasBudgetStructureIdentifier "183262"^^xsd:integer ;
        subjectRDF = URIRef(fstr[BudgetArticleItemInstance])
        predicateRDF = URIRef(fstr[hasBudgetStructureIdentifier])
        BudgetStructureIdentifier = ElementID
        objectRDF = Literal(BudgetStructureIdentifier,datatype=XSD.integer)
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)

    elif ElementTypeIndicator == 'SAO_ID': #A refinement of a budget article item, the budget article subitem brings more detail information on what is being budgeted.

        # Establish budget chapter parent and its value. It is assumed that a budget article sub item always has a budget chapter. Throughout history this has proven to be true. In the future this logic might break down.
        BudgetChapterID = int(dfarticle2['BGG_ID'])
        BudgetChapterInstanceNumber = str(dfbudgetchapter.loc[dfbudgetchapter['ID']==BudgetChapterID]['NR'].values[0])
        BudgetChapterInstance = BudgetStructureIndex[BudgetChapterID]

        # Establish budget policy domain parent and its value (if there is a budget policy domain)
        if pd.notna(dfarticle2['BTN_ID']):
            BudgetPolicyDomainID = int(dfarticle2['BTN_ID'])
            BudgetPolicyDomainInstanceNumber = str(int(dfarticle['BTN_NR'][line_count]))
            BudgetPolicyDomainInstance = BudgetStructureIndex[BudgetPolicyDomainID] #Get RDF identifier for budgetchapter from index
        else:
            BudgetPolicyDomainInstanceNumber=''
            BudgetPolicyDomainID = ''

        # Establish article parent and its value. It is assumed that a budget article sub items always has a budget article. Throughout history this has proven to be true. In the future this logic might break down.
        BudgetArticleID = int(dfarticle2['A_ID'])
        BudgetArticleInstanceNumber = str(int(dfarticle['A_NR'][line_count]))
        BudgetArticleInstance = BudgetStructureIndex[BudgetArticleID] #Get RDF identifier for budget article from index

        # Establish article item parent and its value. It is assumed that a budget article sub items always has a budget article item. Throughout history this has proven to be true. In the future this logic might break down.
        BudgetArticleItemID = int(dfarticle2['AO_ID'])
        BudgetArticleItemInstanceNumber = str(int(dfarticle['AO_NR'][line_count]))
        BudgetArticleItemInstance = BudgetStructureIndex[BudgetArticleItemID] #Get RDF identifier for budget article from index

        # Create Article Sub Item Identifier
        BudgetArticleSubItemInstanceNumber = str(int(dfarticle['SAO_NR'][line_count]))
        BudgetArticleSubItemInstanceName = str(dfarticle['SAO_OMSCHRIJVING'][line_count])
        BudgetArticleSubItemInstance = BudgetChapterPrefix + BudgetChapterInstanceNumber + BudgetPolicyDomainPrefix + BudgetPolicyDomainInstanceNumber + BudgetArticlePrefix + BudgetArticleInstanceNumber + BudgetArticleItemPrefix + BudgetArticleItemInstanceNumber + BudgetArticleSubItemPrefix + BudgetArticleSubItemInstanceNumber + "-" + str(year) + '-' + str(ElementID)
        BudgetStructureIndex.update({ElementID: BudgetArticleSubItemInstance}) #Adding newly created RDF element to index

        # BudgetArticleSubItemInstance is of type BudgetArticleSubItem, e.g. fstr:BASI1000001999-94378 rdf:type fstr:BudgetArticleSubItem
        subjectRDF = URIRef(fstr[BudgetArticleSubItemInstance])
        predicateRDF = URIRef(RDF.type)
        objectRDF = URIRef(fstr[BudgetArticleSubItem])
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)


        if pd.notna(dfarticle2['BTN_ID']):
            BudgetStructureHierarchyIndex.update({ElementID: [BudgetArticleSubItemInstance,BudgetArticleItemInstance, BudgetArticleInstance, BudgetPolicyDomainInstance, BudgetChapterInstance]}) #Adding corresponding budget structure hierarchy of RDF element to index including budgetpolicydomain
        else:
            BudgetStructureHierarchyIndex.update({ElementID: [BudgetArticleSubItemInstance,BudgetArticleItemInstance, BudgetArticleInstance, BudgetChapterInstance]}) #Adding corresponding budget structure hierarchy of RDF element to index without budget policy domain


        # BudgetArticleItemInstance has child value BudgetArticleSubItemInstance, e.g. fstr:BAI0100001999-30658 fstr:hasBudgetStructureChildElement fstr:BASI1000001999-94378
        subjectRDF = URIRef(fstr[BudgetArticleItemInstance])
        predicateRDF = URIRef(fstr[hasBudgetStructureChildElement])
        objectRDF = URIRef(fstr[BudgetArticleSubItemInstance])
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)

        # BudgetArticleSubItemInstance has parent value BudgetArticleItemInstance, e.g. fstr:BASI1000001999-94378 fstr:hasBudgetStructureParentElement fstr:BAI0100001999-30658
        subjectRDF = URIRef(fstr[BudgetArticleSubItemInstance])
        predicateRDF = URIRef(fstr[hasBudgetStructureParentElement])
        objectRDF = URIRef(fstr[BudgetArticleItemInstance])
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)

        # Add name to article sub item, e.g. fstr:BASI1000001999-94378 fstr:hasBudgetStructureElementName "Nog niet toegerekend aan begrotingsart."^^xsd:string
        subjectRDF = URIRef(fstr[BudgetArticleSubItemInstance])
        predicateRDF = URIRef(fstr[hasBudgetStructureElementName])
        objectRDF = Literal(BudgetArticleSubItemInstanceName,datatype=XSD.string)
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)

        # Add number to article sub item, e.g. fstr:BASI1000001999-94378 fstr:hasBudgetStructureElementNumber "0"^^xsd:string
        subjectRDF = URIRef(fstr[BudgetArticleSubItemInstance])
        predicateRDF = URIRef(fstr[hasBudgetStructureElementNumber])
        objectRDF = Literal(BudgetArticleSubItemInstanceNumber,datatype=XSD.string)
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)
        
        # Add BudgetStructureIdentifier (bol_id from IBOS system), e.g. fstr:BASI1000001999-94378 fstr:hasBudgetStructureIdentifier "94378"^^xsd:integer ;
        subjectRDF = URIRef(fstr[BudgetArticleSubItemInstance])
        predicateRDF = URIRef(fstr[hasBudgetStructureIdentifier])
        BudgetStructureIdentifier = ElementID
        objectRDF = Literal(BudgetStructureIdentifier,datatype=XSD.integer)
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)

    elif ElementTypeIndicator == 'INSTRUMENT_ID': #A refinement of a budget article, the budget instrument qualifies what type of financial tool is being budgeted, e.g. a governmental subsidy, purchase, loan or other financial tools the Minister can use to achieve the policy goals.

        # Establish budget chapter parent and its value. It is assumed that a budget instrument always has a budget chapter. Throughout history this has proven to be true. In the future this logic might break down.
        BudgetChapterID = int(dfarticle2['BGG_ID'])
        BudgetChapterInstanceNumber = str(dfbudgetchapter.loc[dfbudgetchapter['ID']==BudgetChapterID]['NR'].values[0])

        # Establish budget policy domain parent and its value (if there is a budget policy domain)
        if pd.notna(dfarticle2['BTN_ID']):
            BudgetPolicyDomainID = int(dfarticle2['BTN_ID'])
            BudgetPolicyDomainInstanceNumber = str(int(dfarticle['BTN_NR'][line_count]))
        else:
            BudgetPolicyDomainInstanceNumber=''
            BudgetPolicyDomainID = ''

        # Establish article parent and its value. It is assumed that a budget instrument always has a budget article. Throughout history this has proven to be true. In the future this logic might break down.
        BudgetArticleID = int(dfarticle2['A_ID'])
        BudgetArticleInstanceNumber = str(int(dfarticle['A_NR'][line_count]))
        BudgetArticleInstance = BudgetStructureIndex[BudgetArticleID] #Get RDF identifier for budget article from index

        # Establish article item parent and its value. It is assumed that a budget instrument always has a budget article item. Throughout history this has proven to be true. In the future this logic might break down.
        BudgetArticleItemID = int(dfarticle2['AO_ID'])
        BudgetArticleItemInstanceNumber = str(int(dfarticle['AO_NR'][line_count]))
        BudgetArticleItemInstance = BudgetStructureIndex[BudgetArticleItemID] #Get RDF identifier for budget article from index

        # Create budget instrument identifier
        BudgetInstrumentInstanceNumber = str(int(dfarticle['INSTRUMENT_NR'][line_count]))
        BudgetInstrumentInstanceName = str(dfarticle['INSTRUMENT_OMSCHRIJVING'][line_count])
        BudgetInstrumentInstance =  BudgetChapterPrefix + BudgetChapterInstanceNumber + BudgetPolicyDomainPrefix + BudgetPolicyDomainInstanceNumber + BudgetArticlePrefix + BudgetArticleInstanceNumber + BudgetArticleItemPrefix + BudgetArticleItemInstanceNumber + BudgetInstrumentPrefix + BudgetInstrumentInstanceNumber + "-"+ str(year) + "-" + str(ElementID)
        BudgetStructureIndex.update({ElementID: BudgetInstrumentInstance}) #Adding newly created RDF element to index

        # BudgetInstrumentInstance is of type BudgetInstrument, e.g. fstr:BI521002020-180623 rdf:type fstr:BudgetInstrument
        subjectRDF = URIRef(fstr[BudgetInstrumentInstance])
        predicateRDF = URIRef(RDF.type)
        objectRDF = URIRef(fstr[BudgetInstrument])
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)

        # BudgetArticleItemInstance has child value BudgetInstrumentInstance, e.g. fstr:BAI052102020-183262 fstr:hasBudgetStructureChildElement fstr:BI521002020-180623
        subjectRDF = URIRef(fstr[BudgetArticleItemInstance])
        predicateRDF = URIRef(fstr[hasBudgetStructureChildElement])
        objectRDF = URIRef(fstr[BudgetInstrumentInstance])
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)

        # BudgetInstrumentInstance has parent value BudgetArticleItemInstance, e.g. fstr:BI521002020-180623 fstr:hasBudgetStructureParentElement fstr:BAI052102020-183262
        subjectRDF = URIRef(fstr[BudgetInstrumentInstance])
        predicateRDF = URIRef(fstr[hasBudgetStructureParentElement])
        objectRDF = URIRef(fstr[BudgetArticleItemInstance])
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)

        # Add name to budget instrument, e.g. fstr:BI521002020-180623 fstr:hasBudgetStructureElementName "Doorberekening Defensie diversen"^^xsd:string
        subjectRDF = URIRef(fstr[BudgetInstrumentInstance])
        predicateRDF = URIRef(fstr[hasBudgetStructureElementName])
        objectRDF = Literal(BudgetInstrumentInstanceName,datatype=XSD.string)
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)

        # Add number to budget instrument, e.g. fstr:BI521002020-180623 fstr:hasBudgetStructureElementNumber "0"^^xsd:string
        subjectRDF = URIRef(fstr[BudgetInstrumentInstance])
        predicateRDF = URIRef(fstr[hasBudgetStructureElementNumber])
        objectRDF = Literal(BudgetInstrumentInstanceNumber,datatype=XSD.string)
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)
        
        # Add BudgetStructureIdentifier (bol_id from IBOS system), e.g. fstr:BI521002020-180623 fstr:hasBudgetStructureIdentifier "180623"^^xsd:integer ;
        subjectRDF = URIRef(fstr[BudgetInstrumentInstance])
        predicateRDF = URIRef(fstr[hasBudgetStructureIdentifier])
        BudgetStructureIdentifier = ElementID
        objectRDF = Literal(BudgetStructureIdentifier,datatype=XSD.integer)
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)

    elif ElementTypeIndicator == 'DETAIL_ID': #A refinement of a budget article and budget instrument, the budget article detail item brings more detail information on what is being budgeted.

        # Establish budget chapter parent and its value. It is assumed that a detail article always has a budget chapter. Throughout history this has proven to be true. In the future this logic might break down.
        BudgetChapterID = int(dfarticle2['BGG_ID'])
        BudgetChapterInstanceNumber = str(dfbudgetchapter.loc[dfbudgetchapter['ID']==BudgetChapterID]['NR'].values[0])
        BudgetChapterInstance = BudgetStructureIndex[BudgetChapterID]

        # Establish budget policy domain parent and its value (if there is a budget policy domain)
        if pd.notna(dfarticle2['BTN_ID']):
            BudgetPolicyDomainID = int(dfarticle2['BTN_ID'])
            BudgetPolicyDomainInstanceNumber = str(int(dfarticle['BTN_NR'][line_count]))
            BudgetPolicyDomainInstance = BudgetStructureIndex[BudgetPolicyDomainID] #Get RDF identifier for budgetchapter from index
        else:
            BudgetPolicyDomainInstanceNumber=''
            BudgetPolicyDomainID = ''

        # Establish article parent and its value. It is assumed that a detail article always has an article. Throughout history this has proven to be true. In the future this logic might break down.
        BudgetArticleID = int(dfarticle2['A_ID'])
        BudgetArticleInstanceNumber = str(int(dfarticle['A_NR'][line_count]))
        BudgetArticleInstance = BudgetStructureIndex[BudgetArticleID] #Get RDF identifier for budget article from index

        # Establish article item parent and its value. It is assumed that a detail article always has an article item. Throughout history this has proven to be true. In the future this logic might break down.
        BudgetArticleItemID = int(dfarticle2['AO_ID'])
        BudgetArticleItemInstanceNumber = str(int(dfarticle['AO_NR'][line_count]))
        BudgetArticleItemInstance = BudgetStructureIndex[BudgetArticleItemID] #Get RDF identifier for budget article item from index

        # Establish instrument parent and its value. It is assumed that a detail article always has an instrument. Throughout history this has proven to be true. In the future this logic might break down.
        BudgetInstrumentID = int(dfarticle2['INSTRUMENT_ID'])
        BudgetInstrumentInstanceNumber = str(int(dfarticle['INSTRUMENT_NR'][line_count]))
        BudgetInstrumentInstance = BudgetStructureIndex[BudgetInstrumentID] #Get RDF identifier for budget instrument from index

        # Create Article Detail Item Identifier
        BudgetArticleDetailItemInstanceNumber = str(int(dfarticle['DETAIL_NR'][line_count]))
        BudgetArticleDetailItemInstanceName = str(dfarticle['DETAIL_OMSCHRIJVING'][line_count])
        BudgetArticleDetailItemInstance = BudgetChapterPrefix + BudgetChapterInstanceNumber + BudgetPolicyDomainPrefix + BudgetPolicyDomainInstanceNumber + BudgetArticlePrefix + BudgetArticleInstanceNumber + BudgetArticleItemPrefix + BudgetArticleItemInstanceNumber + BudgetInstrumentPrefix + BudgetInstrumentInstanceNumber + BudgetArticleDetailItemPrefix + BudgetArticleDetailItemInstanceNumber + "-" + str(year) + "-" + str(ElementID)
        BudgetStructureIndex.update({ElementID: BudgetArticleDetailItemInstance}) #Adding newly created RDF element to index

       # BudgetArticleDetailItemInstance is of type BudgetArticleDetailItem, e.g. fstr:BADI5210002020-180624 rdf:type fstr:BudgetArticleDetailItem
        subjectRDF = URIRef(fstr[BudgetArticleDetailItemInstance])
        predicateRDF = URIRef(RDF.type)
        objectRDF = URIRef(fstr[BudgetArticleDetailItem])
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)


        if pd.notna(dfarticle2['BTN_ID']):
            BudgetStructureHierarchyIndex.update({ElementID: [BudgetArticleDetailItemInstance, BudgetInstrumentInstance, BudgetArticleItemInstance, BudgetArticleInstance, BudgetPolicyDomainInstance, BudgetChapterInstance]}) #Adding corresponding budget structure hierarchy of RDF element to index including budgetpolicydomainID
        else:
            BudgetStructureHierarchyIndex.update({ElementID: [BudgetArticleDetailItemInstance, BudgetInstrumentInstance, BudgetArticleItemInstance, BudgetArticleInstance, BudgetChapterInstance]}) #Adding corresponding budget structure hierarchy of RDF element to index without budget policy domain


        # BudgetInstrumentInstance has child value BudgetArticleDetailItemInstance, e.g. fstr:BI521002020-180623 fstr:hasBudgetStructureChildElement fstr:BADI5210002020-180624
        subjectRDF = URIRef(fstr[BudgetInstrumentInstance])
        predicateRDF = URIRef(fstr[hasBudgetStructureChildElement])
        objectRDF = URIRef(fstr[BudgetArticleDetailItemInstance])
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)

        # BudgetArticleDetailItemInstance has parent value BudgetInstrumentInstance, e.g. fstr:BADI5210002020-180624 fstr:hasBudgetStructureParentElement fstr:BI521002020-180623
        subjectRDF = URIRef(fstr[BudgetArticleDetailItemInstance])
        predicateRDF = URIRef(fstr[hasBudgetStructureParentElement])
        objectRDF = URIRef(fstr[BudgetInstrumentInstance])
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)

        # Add name to article detail item, e.g. fstr:BADI5210002020-180624 fstr:hasBudgetStructureElementName "Doorberekening Defensie diversen"^^xsd:string
        subjectRDF = URIRef(fstr[BudgetArticleDetailItemInstance])
        predicateRDF = URIRef(fstr[hasBudgetStructureElementName])
        objectRDF = Literal(BudgetArticleDetailItemInstanceName,datatype=XSD.string)
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)

        # Add number to article detail item, e.g. fstr:BADI5210002020-180624 fstr:hasBudgetStructureElementNumber "0"^^xsd:string
        subjectRDF = URIRef(fstr[BudgetArticleDetailItemInstance])
        predicateRDF = URIRef(fstr[hasBudgetStructureElementNumber])
        objectRDF = Literal(BudgetArticleDetailItemInstanceNumber,datatype=XSD.string)
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)
        
        # Add BudgetStructureIdentifier (bol_id from IBOS system), e.g. fstr:BADI5210002020-180624 fstr:hasBudgetStructureIdentifier "180624"^^xsd:integer ;
        subjectRDF = URIRef(fstr[BudgetArticleDetailItemInstance])
        predicateRDF = URIRef(fstr[hasBudgetStructureIdentifier])
        BudgetStructureIdentifier = ElementID
        objectRDF = Literal(BudgetStructureIdentifier,datatype=XSD.integer)
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)

    line_count = line_count + 1

print("Done processing rows from IBOS budget structure file")
print("Now serializing graph...")
# Writing resulting RDF-graph to file
print (g.serialize(destination='Script Output/outputstructure.ttl',format='turtle'))
print("Done serializing graph")

# Writing resulting budget structure index to file for lookup purposes; index contains elementID from IBOS and the concerning generated counterpart identifier in RDF
print("Now writing to files...")
import csv
with open("Script Input/budgetstructureindex.csv","w", newline='', encoding='utf-8') as BudgetStructureIndexFile:
    BudgetStructureIndexFileWriter = csv.writer(BudgetStructureIndexFile, escapechar=' ',delimiter=';',quoting=csv.QUOTE_NONE)
    for key, val in BudgetStructureIndex.items():
        BudgetStructureIndexFileWriter.writerow([key,val])

with open("Script Input/budgetstructurehierarchyindex.csv","w", newline='', encoding='utf-8') as BudgetStructureHierarchyIndexFile:
    BudgetStructureHierarchyIndexFileWriter = csv.writer(BudgetStructureHierarchyIndexFile, escapechar=' ',delimiter=';',quoting=csv.QUOTE_NONE)
    for key, val in BudgetStructureHierarchyIndex.items():
        BudgetStructureHierarchyIndexFileWriter.writerow([key,val])

print("Done writing to files")
print ("End of program")
