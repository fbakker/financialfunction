# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a script file to convert data in CSV-format regarding individual budgetary transactions and aggregated budgetary balance statements from the IBOS application to Turtle script (RDF).
IBOS being the budgetary administrative system of the Dutch government.

The source data file contains per row one individual budgetary transaction or one aggregated budgetary balance statement. Each row references several dimensions (budget category, budget domain sector, budget application effect and more) to contexualize the budget transaction or statement.
Each row references a 'BOL_ID' which is a reference to the budget structure element (e.g. budget chapter, budget article, budget instrument and more) and its corresponding budget structure hierarchy. This means
you have to run first another script (not contained in this file) called 'BudgetStructure' so that the current script can lookup budget structure references from the results of that script.

Needed as input:

1. Budget Mutations table from IBOS
2. Budget Structure Hierarchy Index "budgetstructurehierarchyindex.csv" (generated by script BudgetStructure.py)

Make sure the file is present in the input folder. Then run the script. The script will read the file, process row by row (and while this, it will additionally read the second file to establish budget structure hierarchy). After processing row by row, it will serialize the graph and write the end result to file. For 72000 IBOS transactions it takes around 40 minutes to generate the file.
"""

#packages
import pandas as pd # Importing pandas for manipulating and generating dataframes
import pyodbc # Importing pyodbc for connecting to ODBC database
import ast # Importing ast in order to transform a string-formatted list to Python-list

# Imporing RDFlib for generating or manipulating RDF data.
import rdflib
from rdflib import Graph, URIRef, BNode, Literal, Namespace
from rdflib.namespace import RDF, FOAF, XSD


#SQL database connection 
conn = pyodbc.connect(
    r'DRIVER={ODBC Driver 17 for SQL Server};'
    r'SERVER=mfipapp075,50002;'
    r'DATABASE=RHBP;'
    r'Trusted_Connection=yes;'
)
cursor = conn.cursor()

# Dictionary definitions for further lookup purposes (converting certain codes used in IBOS.)


# IBOS budgetary mutations and budgetary balance statements budget at least one year or more years. To calculate the exact year the budgetary mutation or balance statement
# is referring one has to know the relative distance from the current budget year, e.g. 'BEDRAG_TPLUS1' means that the amount that is contained in the table column
# 'BEDRAG_TPLUS1' is appearantly a budget amount for the year AFTER the current budget year. When we know that the current budget year is 2018, we can calculate the referenced
# amount is itself referring to the next budget year, being 2018 + 1 = 2019. This dict translates the table column headers to the right integer needed to do that calculation.
YearCalculationDict = {
        'KAS_BEDR_JR_T_M1': '-1',
        'KAS_BEDR_JR_T' : '0',
        'KAS_BEDR_JR_T_P1': '1',
        'KAS_BEDR_JR_T_P2': '2',
        'KAS_BEDR_JR_T_P3': '3',
        'KAS_BEDR_JR_T_P4': '4',
        'KAS_BEDR_JR_T_P5': '5',
        'KAS_BEDR_JR_T_P6': '6',
        'KAS_BEDR_JR_T_P7': '7',
        'KAS_BEDR_JR_T_P8': '8',
        'KAS_BEDR_JR_T_P9': '9',
        'KAS_BEDR_JR_T_P10': '10',
        'KAS_BEDR_JR_T_P11': '11',
        'KAS_BEDR_JR_T_P12': '12',
        'KAS_BEDR_JR_T_P13': '13',
        'KAS_BEDR_JR_T_P14': '14',
        'KAS_BEDR_JR_T_P15': '15',
        'KAS_BEDR_JR_T_P16': '16',
        'KAS_BEDR_JR_T_P17': '17',
        'KAS_BEDR_JR_T_P18': '18',
        'KAS_BEDR_JR_T_P19': '19',
        'VERPL_BEDR_JR_T_M1':'-1',
        'VERPL_BEDR_JR_T':'0',
        'VERPL_BEDR_JR_T_P1':'1',
        'VERPL_BEDR_JR_T_P2':'2',
        'VERPL_BEDR_JR_T_P3':'3',
        'VERPL_BEDR_JR_T_P4':'4',
        'VERPL_BEDR_JR_T_P5':'5',
        'VERPL_BEDR_JR_T_P6':'6',
        'VERPL_BEDR_JR_T_P7':'7',
        'VERPL_BEDR_JR_T_P8':'8',
        'VERPL_BEDR_JR_T_P9':'9',
        'VERPL_BEDR_JR_T_P10':'10',
        'VERPL_BEDR_JR_T_P11':'11',
        'VERPL_BEDR_JR_T_P12':'12',
        'VERPL_BEDR_JR_T_P13':'13',
        'VERPL_BEDR_JR_T_P14':'14',
        'VERPL_BEDR_JR_T_P15':'15',
        'VERPL_BEDR_JR_T_P16':'16',
        'VERPL_BEDR_JR_T_P17':'17',
        'VERPL_BEDR_JR_T_P18':'18',
        'VERPL_BEDR_JR_T_P19':'19',
        }

BudgetChapterTypeDict = {
        'B': 'DepartmentBudget',
        'F': 'BudgetFund',
        'O': 'Non-DepartmentBudget',
        'D': 'Dummy',
        'P': 'Contribution',
        'X': 'NotInBudgetSystem',
        }

SectorDict = {
        'R': 'NationalBudgetNarrowSense',
        'S': 'SocialSecurity',
        'Z': 'HealthCare',
        'N': 'NotPertainingToAnyBudgetaryCeiling',
        'X': 'Conversion',
        }

ApplicationEffectDict = {
        'X': 'NotOtherwiseSpecified',
        'D': 'ZeroSumNetting',
        'G': 'GeneralBudgetaryApplication',
        'O': 'TransferToOtherBudgetChapter',
        'S': 'SpecificCompensation',
        }

BudgetingSourceDocumentDict = {
        'OW': 'DraftInitialBudgetBill',
        'O1': 'DraftFirstSuppletoryBill',
        'O2': 'DraftSecondSuppletoryBill',
        'OS': 'DraftFinalBudgetBill',
        'AW': 'InitialBudgetBillOfAmendment',
        'NW': 'InitialBudgetMemorandumOfRevision',
        'I1': 'DraftIncidentalSuppletoryBillBeforeFirstSuppletoryBill',
        'I2': 'DraftIncidentalSuppletoryBillBeforeSecondSuppletoryBill',
        'I3': 'DraftIncidentalSuppletoryBillBeforeFinalBudgetBill',
        'J1': 'IncidentalSuppletoryBillBeforeFirstSuppletoryBillBillOfAmendment',
        'J2': 'IncidentalSuppletoryBillBeforeSecondSuppletoryBillBillOfAmendment',
        'J3': 'IncidentalSuppletoryBillBeforeFinalBudgetBillBillOfAmendment',
        'W1': 'IncidentalSuppletoryBillBeforeFirstSuppletoryBillMemorandumOfRevision',
        'W2': 'IncidentalSuppletoryBillBeforeSecondSuppletoryBillMemorandumOfRevision',
        'W3': 'IncidentalSuppletoryBillBeforeFinalBudgetBillMemorandumOfRevision',
        'A1': 'FirstSuppletoryBillBillOfAmendment',
        'N1': 'FirstSuppletoryBillMemorandumOfRevision',
        'A2': 'SecondSuppletoryBillBillOfAmendment',
        'N2': 'SecondSuppletoryBillMemorandumOfRevision',
        'AS': 'FinalBudgetBillBillOfAmendment',
        'NS': 'FinalBudgetBillMemorandumOfRevision',
        }

CategoryOfBudgetaryMutationDict = {
        '0': 'RelevantForFinancingDeficit',
        '1': 'InternationalCooperation',
        '2': 'NonRelevantForFinancingDeficit',
        '3': 'BigCitiesPolicy',
        '4': 'Conversion',
        '5': 'CumulativeFundBalance',
        '6': 'IncentiveScheme',
        }

ReasonForBudgetaryMutationDict = {
        'X': 'NotApplicable',
        'B': 'PolicyBased',
        'A': 'Autonomous',
        'T': 'Technical',
        }

TransactionalDirectionDict = {
        'U': 'Outgoing',
        'O': 'Incoming',
        }

CashCommitmentSynchronizationDict = {
        'J': 'Synchronous',
        'N': 'Asynchronous',
        }

BudgetAdjustmentTypeDict = {
        'B1': 'BudgetMemorandumBalanceFact',
        }

BudgetStructureTypeDict = {
        'BudgetChapter': 'referencesBudgetChapter',
        'BudgetPolicyDomain': 'referencesBudgetPolicyDomain',
        'BudgetArticle': 'referencesBudgetArticle',
        'BudgetArticleItem': 'referencesBudgetArticleItem',
        'BudgetArticleSubItem': 'referencesBudgetArticleSubItem',
        'BudgetArticleDetailItem': 'referencesBudgetArticleDetailItem',
        'BudgetInstrument': 'referencesBudgetInstrument',
        }

# Function with dictionary to establish budget sector based on code given through argument x
def fSector(x):
    return SectorDict.get(x,'UnspecifiedSector')

# Function with dictionary to establish budgetary application effect based on code given through argument x
def fApplicationEffect(x):
    return ApplicationEffectDict.get(x,'UnspecifiedApplicationEffect')

# Function with dictionary to establish budgeting source document based on code given through argument x
def fBudgetingSourceDocumentDict(x):
    return BudgetingSourceDocumentDict.get(x,'UnspecifiedBudgetingSourceDocument')

# Function with dictionary to establish category of budgetary mutation based on code given through argument x
def fCategoryOfBudgetaryMutationDict(x):
    return CategoryOfBudgetaryMutationDict.get(x,'UnspecifiedCategoryOfBudgetaryMutation')

# Function with dictionary to establish reason for the budgetary mutation based on code given through argument x
def fReasonForBudgetaryMutationDict(x):
    return ReasonForBudgetaryMutationDict.get(x,'UnspecifiedReasonForBudgetaryMutation')

# Function with dictionary to establish whether a budget adjustment in IBOS entails an aggregated balance statement or an individual transaction based on code given through argument x
def fBudgetAdjustmentTypeDict (x):
    return BudgetAdjustmentTypeDict.get(x,'BudgetaryMutationFact')



# Initializing dataframe from file containing budgetary transactions
dfibos = pd.read_sql("select * from ris_begrotingsmutaties bge where bge.[STATUS] = 'A' and bge.[MILJ_NOTA_JR] = 2021;", conn, coerce_float=False) 


# Initializing dataframe from file containing the RDF-converted budget structure hierarchy for the key element_id mentioned in the budget adjustment row
dfbudgetstructurehierarchyindex = pd.read_csv("Script Input/budgetstructurehierarchyindex.csv",sep=";",encoding="utf-8",dtype={'BUDGET_STRUCTURE_ELEMENT_ID':str, 'HIERARCHY':str})

# Define entities with type string for re-usability and adaptability purposes. In addition this may help in making python code in the section where RDF triples are generated more readable.
# RDF Prefixes
# RDF Class
# RDF Objects
# RDF Property

# RDF Prefix
BudgetAdjustmentPrefix = 'BAT'
NationalBudgetFactCashPrefix = 'BFCA'
NationalBudgetFactCommitmentPrefix = 'BFCO'
MonetaryAmountPrefix ='MA'
BudgetStructureReferencePrefix = 'BSR'
BudgetStructurePrefix = 'BS'
AnnualBudgetStructurePrefix = 'ABS'
BudgetChapterPrefix= 'BC'
BudgetArticlePrefix= 'BA'
BudgetArticleItemPrefix= 'BAI'
BudgetArticleSubItemPrefix='BASI'
BudgetBeginPrefix = 'BB'
BudgetEndPrefix = 'BE'
BudgetDatePrefix = 'BD'
ActiveBudgetStructurePeriodPrefix = 'ABSP'
DateTimeDescriptionPrefix = 'DTD'
DateBeginPrefix = 'DATEBEGIN'
DateEndPrefix ='DATEEND'
BudgetChapterPolicyAgendaPrefix = 'BCPA'
BudgetPolicyDomainPrefix = 'BPD'
BudgetInstrumentPrefix = 'BI'
BudgetArticleDetailItemPrefix = 'BADI'
BudgetMemorandumPeriodPrefix = 'BM'
NationalBudgetPeriodPrefix = 'BP'
NationalBudgetFactPeriodPrefix = 'BFP'

# RDF Class
NationalBudgetFact = 'NationalBudgetFact'

# RDF Object
UnitYear = 'unitYear'
CurrencyInstance = 'EUR'
DegreeOfCertainty = 'Certainty'
BudgetAdjustment = 'BudgetAdjustment'
TransactionalRealisationPhase = 'TransactionalRealisationPhase'

# RDF Property
hasNationalBudgetFact = 'hasNationalBudgetFact'
hasBudgetAdjustmentIdentifier = 'hasBudgetAdjustmentIdentifier'
hasTransactionalRealisationPhase = 'hasTransactionalRealisationPhase'
hasDateTimeDescription ='hasDateTimeDescription'
hasTransactionalDirection = 'hasTransactionalDirection'
hasCashCommitmentSynchronization = 'hasCashCommitmentSynchronization'
hasBudgetAdjustmentDescription = 'hasBudgetAdjustmentDescription'
hasBudgetAdjustmentComments = 'hasBudgetAdjustmentComments'
hasLegalFramework = 'hasLegalFramework'
hasBudgetPeriod = 'hasBudgetPeriod'
hasMonetaryAmount = 'hasMonetaryAmount'
hasCurrency = 'hasCurrency'
rdfType = 'rdf:type'
hasBeginning = 'hasBeginning'
hasEnd = 'hasEnd'
inXSDDate = 'inXSDDate'
unitType = 'unitType'
Year = 'year'
hasValue ='hasValue'
hasBudgetaryApplicationEffect = 'hasBudgetaryApplicationEffect'
hasBudgetingSourceDocument = 'hasBudgetingSourceDocument'
hasCategoryOfBudgetaryMutation = 'hasCategoryOfBudgetaryMutation'
hasReasonForBudgetaryMutation = 'hasReasonForBudgetaryMutation'
hasBudgetDomainSector = 'hasBudgetDomainSector'
hasDegreeOfCertainty = 'hasDegreeOfCertainty'
referencesBudgetStructureElement = 'referencesBudgetStructureElement'
hasBudgetMemorandumPeriod = 'hasBudgetMemorandumPeriod'
hasNationalBudgetPeriod = 'hasNationalBudgetPeriod'
hasNationalBudgetFactPeriod = 'hasNationalBudgetFactPeriod'
hasBudgetAdjustmentType = 'hasBudgetAdjustmentType'
hasBudgetAdjustmentNumber = 'hasBudgetAdjustmentNumber'
hasBudgetAdjustmentCGroup = 'hasBudgetAdjustmentCGroup'
hasBudgetAdjustmentDGroup = 'hasBudgetAdjustmentDGroup'



# Define graph, namespace and prefix
g = Graph()
fbud = Namespace("https://www.rijksfinancien.nl/rijksbegroting/")
g.bind(prefix='fbud', namespace=fbud)
fstr = Namespace("https://www.rijksfinancien.nl/rijksbegrotingsstructuur/")
g.bind(prefix='fstr', namespace=fstr)
ffact = Namespace("https://www.rijksfinancien.nl/financieelfeit/")
g.bind(prefix='ffact', namespace=ffact)
time = Namespace("http://www.w3.org/2006/time#/")
g.bind(prefix='time', namespace=time)

# Create a budget adjustment transaction for each row in the source file consisting of budgetary transactions and budgetary balance statements in IBOS. IBOS being the budgetary administrative system of the Dutch government.
print("Now processing rows...")
line_count = 0
for row in dfibos.itertuples(index=False):
    print(line_count) # makes it easier to follow progress
    UniqueIdentifier = str(dfibos["VOLG_NR"][line_count])
    BudgetAdjustmentInstance = BudgetAdjustmentPrefix + UniqueIdentifier #every transaction in IBOS has a unique identifying number. Using this to create an unique IRI.

    # BudgetAdjustmentInstance is of type BudgetAdjustment, e.g. fbud:BA01 rdf:type fbud:BudgetAdjustment
    subjectRDF =  URIRef(fbud[BudgetAdjustmentInstance])
    predicateRDF = URIRef(RDF.type)
    objectRDF = URIRef(fbud[BudgetAdjustment])
    triple = (subjectRDF, predicateRDF, objectRDF)
    g.add(triple)
    
    # BudgetAdjustmentInstance has BudgetAdjustmentIdentifier, e.g. fbud:BA385485737 fbud:hasBudgetAdjustmentIdentifier "385485737"^xsd:integer
    BudgetAdjustmentIdentifierInstance = UniqueIdentifier
    subjectRDF =  URIRef(fbud[BudgetAdjustmentInstance])
    predicateRDF = URIRef(fbud[hasBudgetAdjustmentIdentifier])
    objectRDF = Literal(BudgetAdjustmentIdentifierInstance, datatype=XSD.integer)
    triple = (subjectRDF, predicateRDF, objectRDF)
    g.add(triple)

    # BudgetAdjustmentInstance has BudgetDomainSectorInstance, e.g. fbud:BA01 fbud:hasBudgetDomainSector fbud:NationalBudgetNarrowSense
    BudgetDomainSectorInstance = fSector(str(dfibos['CPS_BDR_CODE'][line_count]))
    subjectRDF =  URIRef(fbud[BudgetAdjustmentInstance])
    predicateRDF = URIRef(fbud[hasBudgetDomainSector])
    objectRDF = URIRef(fbud[BudgetDomainSectorInstance])
    triple = (subjectRDF, predicateRDF, objectRDF)
    g.add(triple)

    # BudgetAdjustmentInstance has BudgetaryApplicationEffectInstance, e.g. fbud:BA01 fbud:hasBudgetApplicationEffect fbud:GeneralBudgetaryApplication
    BudgetaryApplicationEffectInstance = fApplicationEffect(str(dfibos['VRW_CODE'][line_count]))
    subjectRDF =  URIRef(fbud[BudgetAdjustmentInstance])
    predicateRDF = URIRef(fbud[hasBudgetaryApplicationEffect])
    objectRDF = URIRef(fbud[BudgetaryApplicationEffectInstance])
    triple = (subjectRDF, predicateRDF, objectRDF)
    g.add(triple)

    # BudgetAdjustmentInstance has BudgetingSourceDocumentInstance, e.g. fbud:BA01 fbud:hasBudgetSourceDocumentInstance fbud:AdoptedFirstSuppletoryBill
    BudgetingSourceDocumentInstance = fBudgetingSourceDocumentDict(str(dfibos['BGK_CODE'][line_count]))
    subjectRDF =  URIRef(fbud[BudgetAdjustmentInstance])
    predicateRDF = URIRef(fbud[hasBudgetingSourceDocument])
    objectRDF = URIRef(fbud[BudgetingSourceDocumentInstance])
    triple = (subjectRDF, predicateRDF, objectRDF)
    g.add(triple)

    # BudgetAdjustmentInstance has CategoryOfBudgetaryMutationInstance, e.g. fbud:BA01 fbud:hasCategoryOfBudgetaryMutationInstance fbud:RelevantForFinancingDeficit
    CategoryOfBudgetaryMutationInstance = fCategoryOfBudgetaryMutationDict(str(dfibos['CPS_CTE_CODE'][line_count]))
    subjectRDF =  URIRef(fbud[BudgetAdjustmentInstance])
    predicateRDF = URIRef(fbud[hasCategoryOfBudgetaryMutation])
    objectRDF = URIRef(fbud[CategoryOfBudgetaryMutationInstance])
    triple = (subjectRDF, predicateRDF, objectRDF)
    g.add(triple)

    # BudgetAdjustmentInstance has ReasonForBudgetaryMutation, e.g. fbud:BA01 fbud:hasReasonForBudgetaryMutation fbud:Technical
    ReasonForBudgetaryMutationInstance = fReasonForBudgetaryMutationDict(str(dfibos['OZK_CODE'][line_count]))
    subjectRDF =  URIRef(fbud[BudgetAdjustmentInstance])
    predicateRDF = URIRef(fbud[hasReasonForBudgetaryMutation])
    objectRDF = URIRef(fbud[ReasonForBudgetaryMutationInstance])
    triple = (subjectRDF, predicateRDF, objectRDF)
    g.add(triple)

    # BudgetAdjustmentInstance has BudgetAdjustmentType, e.g. fbud:BA01 fbud:hasBudgetAdjustmentType fbud:BudgetaryMutationFact
    BudgetAdjustmentTypeInstance = fBudgetAdjustmentTypeDict(str(dfibos['BCE_NOTA_CODE'][line_count]))
    subjectRDF =  URIRef(fbud[BudgetAdjustmentInstance])
    predicateRDF = URIRef(fbud[hasBudgetAdjustmentType])
    objectRDF = URIRef(fbud[BudgetAdjustmentTypeInstance])
    triple = (subjectRDF, predicateRDF, objectRDF)
    g.add(triple)


    # BudgetAdjustmentInstance has CashCommitmentSynchronization, e.g. fbud:BA01 fbud:hasCashCommitmentSynchronization fbud:Synchronous
    CashCommitmentSynchronizationInstance = CashCommitmentSynchronizationDict[dfibos["KAS_VERPL_IND"][line_count]]
    subjectRDF =  URIRef(fbud[BudgetAdjustmentInstance])
    predicateRDF = URIRef(fbud[hasCashCommitmentSynchronization])
    objectRDF = URIRef(fbud[CashCommitmentSynchronizationInstance])
    triple = (subjectRDF, predicateRDF, objectRDF)
    g.add(triple)

    # BudgetAdjustmentInstance has BudgetAdjustmentNumber, e.g. fbud:BA01 fbud:hasBudgetAdjustmentNumber 01
    BudgetAdjustmentNumberInstance = str(dfibos['MUT_NR'][line_count])
    subjectRDF =  URIRef(fbud[BudgetAdjustmentInstance])
    predicateRDF = URIRef(fbud[hasBudgetAdjustmentNumber])
    objectRDF = Literal(BudgetAdjustmentNumberInstance, datatype=XSD.integer)
    triple = (subjectRDF, predicateRDF, objectRDF)
    g.add(triple)

    # BudgetAdjustmentInstance has BudgetAdjustmentDescription, e.g. fbud:BA01 fbud:hasBudgetAdjustmentDescription "Classified"^^xsd:string
    BudgetAdjustmentDescriptionInstance = str(dfibos['OMSCHRIJVING'][line_count])
    BudgetAdjustmentDescriptionInstance = "Classified" #for now resetting the variable due to security reasons. In the future this line can be deleted)
    subjectRDF =  URIRef(fbud[BudgetAdjustmentInstance])
    predicateRDF = URIRef(fbud[hasBudgetAdjustmentDescription])
    objectRDF = Literal(BudgetAdjustmentDescriptionInstance, datatype=XSD.string)
    triple = (subjectRDF, predicateRDF, objectRDF)
    g.add(triple)

    # BudgetAdjustmentInstance has BudgetAdjustmentComments, e.g. fbud:BA01 fbud:hasBudgetAdjustmentComments "Classified"^^xsd:string
    BudgetAdjustmentCommentsInstance = str(dfibos['TOELICHTING'][line_count])
    BudgetAdjustmentCommentsInstance = "Classified" #for now resetting the variable due to security reasons. In the future this line can be deleted)
    subjectRDF =  URIRef(fbud[BudgetAdjustmentInstance])
    predicateRDF = URIRef(fbud[hasBudgetAdjustmentComments])
    objectRDF = Literal(BudgetAdjustmentCommentsInstance, datatype=XSD.string)
    triple = (subjectRDF, predicateRDF, objectRDF)
    g.add(triple)

    # BudgetAdjustmentInstance has BudgetC-Group, e.g. fbud:BA01 fbud:hasBudgetAdjustmentCGroupInstance fbud:C180
    if pd.notna(dfibos['GRP_NR'][line_count]): #skip this triple if there is no value
        BudgetAdjustmentCGroupInstance = "CGROUP/id/" + str(int(dfibos['GRP_NR'][line_count]))
        subjectRDF =  URIRef(fbud[BudgetAdjustmentInstance])
        predicateRDF = URIRef(fbud[hasBudgetAdjustmentCGroup])
        objectRDF = URIRef(fbud[BudgetAdjustmentCGroupInstance])
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)

    # BudgetAdjustmentInstance has BudgetD-Group, e.g. fbud:BA01 fbud:hasBudgetAdjustmentDGroupInstance fbud:D19
    if pd.notna(dfibos['GRP_BRON_DEP'][line_count]): #skip this triple if there is no value
        BudgetAdjustmentDGroupInstance = "DGROUP/id/" + str(int(dfibos['GRP_NR_DEP'][line_count]))
        subjectRDF =  URIRef(fbud[BudgetAdjustmentInstance])
        predicateRDF = URIRef(fbud[hasBudgetAdjustmentDGroup])
        objectRDF = URIRef(fbud[BudgetAdjustmentDGroupInstance])
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)

    # BudgetAdjustmentInstance references multiple hierarchy budget structure elements, budget chapter, budget article, budget instrument et cetera, e.g. fbud:BA01 fbud:referencesBudgetStructureElement fstr:BASI6344512019-167452
    ElementID = str(dfibos['BOL_ID'][line_count])
    try: #Being careful here as it has happened that budget transactions contain an ElementID that is not present in the budget structure.
        BudgetStructureHierarchyList = dfbudgetstructurehierarchyindex.loc[dfbudgetstructurehierarchyindex['BUDGET_STRUCTURE_ELEMENT_ID']==ElementID]['HIERARCHY'].values[0]
        BudgetStructureHierarchyList = ast.literal_eval(BudgetStructureHierarchyList)
        for BudgetStructureReferenceElementInstance in BudgetStructureHierarchyList:
              subjectRDF =  URIRef(fbud[BudgetAdjustmentInstance])
              predicateRDF = URIRef(fbud[referencesBudgetStructureElement])
              objectRDF = URIRef(fstr[BudgetStructureReferenceElementInstance])
              triple = (subjectRDF, predicateRDF, objectRDF)
              g.add(triple)
    except IndexError:
        BudgetStructureReferenceElementInstance = 'UnknownBudgetStructureReferenceID-'+str(ElementID)
        subjectRDF =  URIRef(fbud[BudgetAdjustmentInstance])
        predicateRDF = URIRef(fbud[referencesBudgetStructureElement])
        objectRDF = URIRef(fstr[BudgetStructureReferenceElementInstance])
        triple = (subjectRDF, predicateRDF, objectRDF)
        g.add(triple)

    # BudgetAdjustmentInstance has BudgetMemorandumPeriod, e.g. fbud:BA01 fbud:hasBudgetMemorandumPeriod fstr:BM2019
    BudgetMemorandumYear = str(dfibos["MILJ_NOTA_JR"][line_count])
    BudgetMemorandumPeriodInstance = BudgetMemorandumPeriodPrefix + str(BudgetMemorandumYear)
    subjectRDF =  URIRef(fbud[BudgetAdjustmentInstance])
    predicateRDF = URIRef(fbud[hasBudgetMemorandumPeriod])
    objectRDF = URIRef(fstr[BudgetMemorandumPeriodInstance])
    triple = (subjectRDF, predicateRDF, objectRDF)
    g.add(triple)

    # BudgetAdjustmentInstance has NationalBudgetPeriod, e.g. fbud:BA01 fbud:hasNationalBudgetPeriod fstr:BP2019
    NationalBudgetYear = str(dfibos["BGK_BEGR_JR"][line_count])
    NationalBudgetPeriodInstance = NationalBudgetPeriodPrefix + str(NationalBudgetYear)
    subjectRDF =  URIRef(fbud[BudgetAdjustmentInstance])
    predicateRDF = URIRef(fbud[hasNationalBudgetPeriod])
    objectRDF = URIRef(fstr[NationalBudgetPeriodInstance])
    triple = (subjectRDF, predicateRDF, objectRDF)
    g.add(triple)

    # Identifying and creating the underlying national budget facts - budgeted cashflow
    dfbudgetfacts = dfibos.loc[line_count] [['KAS_BEDR_JR_T_M1', 'KAS_BEDR_JR_T', 'KAS_BEDR_JR_T_P1', 'KAS_BEDR_JR_T_P2', 'KAS_BEDR_JR_T_P3', 'KAS_BEDR_JR_T_P4', 'KAS_BEDR_JR_T_P5', 'KAS_BEDR_JR_T_P6', 'KAS_BEDR_JR_T_P7', 'KAS_BEDR_JR_T_P8', 'KAS_BEDR_JR_T_P9', 'KAS_BEDR_JR_T_P10', 'KAS_BEDR_JR_T_P11', 'KAS_BEDR_JR_T_P12', 'KAS_BEDR_JR_T_P13', 'KAS_BEDR_JR_T_P14', 'KAS_BEDR_JR_T_P15', 'KAS_BEDR_JR_T_P16', 'KAS_BEDR_JR_T_P17', 'KAS_BEDR_JR_T_P18', 'KAS_BEDR_JR_T_P19']].copy()
    for item in dfbudgetfacts.iteritems():
            if pd.notna(item[1]):
                NationalBudgetFactYear = str(int(BudgetMemorandumYear)+int(YearCalculationDict[item[0]]))
                NationalBudgetFactInstance = NationalBudgetFactCashPrefix + UniqueIdentifier + "-" + str(line_count) + '-' + NationalBudgetFactYear

                # BudgetAdjustmentInstance has NationalBudgetFactInstance, e.g. fbud:BA01 fbud:hasNationalBudgetFact fbud:BFCA2180-2024
                subjectRDF =  URIRef(fbud[BudgetAdjustmentInstance])
                predicateRDF = URIRef(fbud[hasNationalBudgetFact])
                objectRDF = URIRef(fbud[NationalBudgetFactInstance])
                triple = (subjectRDF, predicateRDF, objectRDF)
                g.add(triple)

                # NationalBudgetFactInstance is of type NationalBudgetfact, e.g. fbud:BFCA2180-2024 rdf:type fbud:NationalBudgetFact
                subjectRDF =  URIRef(fbud[NationalBudgetFactInstance])
                predicateRDF = URIRef(RDF.type)
                objectRDF = URIRef(fbud[NationalBudgetFact])
                triple = (subjectRDF, predicateRDF, objectRDF)
                g.add(triple)

                # NationalBudgetFactInstance has type TransactionalRealisationPhase, e.g. fbud:BFCA2180-2024 fbud:hasTransactionalRealisationPhase fbud:Cashflow
                TransactionalRealisationPhaseInstance = 'CashFlow'
                subjectRDF =  URIRef(fbud[NationalBudgetFactInstance])
                predicateRDF = URIRef(fbud[hasTransactionalRealisationPhase])
                objectRDF = URIRef(fbud[TransactionalRealisationPhaseInstance])
                triple = (subjectRDF, predicateRDF, objectRDF)
                g.add(triple)

                # NationalBudgetFactInstance has Monetary Amount, e.g. fbud:BFCA2180-2024 ffact:hasMonetaryAmount ffact:MA2180-2024
                MonetaryAmountInstance = MonetaryAmountPrefix + UniqueIdentifier + "-" + str(line_count) + '-' + NationalBudgetFactYear
                subjectRDF =  URIRef(fbud[NationalBudgetFactInstance])
                predicateRDF = URIRef(ffact[hasMonetaryAmount])
                objectRDF = URIRef(ffact[MonetaryAmountInstance])
                triple = (subjectRDF, predicateRDF, objectRDF)
                g.add(triple)

                # NationalBudgetFactInstance has National Budget Period, e.g. fbud:BFCA2180-2024 fbud:hasNationalBudgetPeriod fstr:BP2018
                NationalBudgetPeriodFactInstance = NationalBudgetFactPeriodPrefix + NationalBudgetFactYear
                subjectRDF =  URIRef(fbud[NationalBudgetFactInstance])
                predicateRDF = URIRef(fbud[hasNationalBudgetFactPeriod])
                objectRDF = URIRef(fstr[NationalBudgetPeriodFactInstance])
                triple = (subjectRDF, predicateRDF, objectRDF)
                g.add(triple)

                # NationalBudgetFactInstance has Transactional Direction, e.g. fbud:BFCA2180-2024 fbud:hasTransactionalDirection fbud:Incoming
                TransactionalDirection = TransactionalDirectionDict[dfibos["UITG_ONTV_IND"][line_count]]
                subjectRDF =  URIRef(fbud[NationalBudgetFactInstance])
                predicateRDF = URIRef(fbud[hasTransactionalDirection])
                objectRDF = URIRef(fbud[TransactionalDirection])
                triple = (subjectRDF, predicateRDF, objectRDF)
                g.add(triple)

                # Monetary Amount has Currency, e.g. ffact:MA2180-2024 ffact:hasCurrency ffact:EUR
                subjectRDF =  URIRef(ffact[MonetaryAmountInstance])
                predicateRDF = URIRef(ffact[hasCurrency])
                objectRDF = URIRef(ffact[CurrencyInstance])
                triple = (subjectRDF, predicateRDF, objectRDF)
                g.add(triple)

                # Monetary Amount has Value, e.g. ffact:MA2180-2024 ffact:hasValue "580290"^^xsd:integer
                subjectRDF =  URIRef(ffact[MonetaryAmountInstance])
                predicateRDF = URIRef(ffact[hasValue])
                objectRDF = Literal(int(item[1]),datatype=XSD.integer)
                triple = (subjectRDF, predicateRDF, objectRDF)
                g.add(triple)

    # Identifying and creating the underlying national budget facts - budgeted commitments
    dfbudgetfacts = dfibos.loc[line_count] [['VERPL_BEDR_JR_T_M1','VERPL_BEDR_JR_T','VERPL_BEDR_JR_T_P1','VERPL_BEDR_JR_T_P2','VERPL_BEDR_JR_T_P3','VERPL_BEDR_JR_T_P4','VERPL_BEDR_JR_T_P5','VERPL_BEDR_JR_T_P6','VERPL_BEDR_JR_T_P7','VERPL_BEDR_JR_T_P8','VERPL_BEDR_JR_T_P9','VERPL_BEDR_JR_T_P10','VERPL_BEDR_JR_T_P11','VERPL_BEDR_JR_T_P12','VERPL_BEDR_JR_T_P13','VERPL_BEDR_JR_T_P14','VERPL_BEDR_JR_T_P15','VERPL_BEDR_JR_T_P16','VERPL_BEDR_JR_T_P17','VERPL_BEDR_JR_T_P18','VERPL_BEDR_JR_T_P19']].copy()
    for item in dfbudgetfacts.iteritems():
            if pd.notna(item[1]):
                NationalBudgetFactYear = str(int(BudgetMemorandumYear)+int(YearCalculationDict[item[0]]))
                NationalBudgetFactInstance = NationalBudgetFactCommitmentPrefix + UniqueIdentifier + "-" + str(line_count) + '-' + NationalBudgetFactYear

                # BudgetAdjustmentInstance has NationalBudgetFact, e.g. fbud:BA01 fbud:hasNationalBudgetFact fbud:BFCO30720-2033
                subjectRDF =  URIRef(fbud[BudgetAdjustmentInstance])
                predicateRDF = URIRef(fbud[hasNationalBudgetFact])
                objectRDF = URIRef(fbud[NationalBudgetFactInstance])
                triple = (subjectRDF, predicateRDF, objectRDF)
                g.add(triple)

                # NationalBudgetFactInstance is of type NationalBudgetfact, e.g. fbud:BFCO30720-2033 rdf:type fbud:NationalBudgetFact
                subjectRDF =  URIRef(fbud[NationalBudgetFactInstance])
                predicateRDF = URIRef(RDF.type)
                objectRDF = URIRef(fbud[NationalBudgetFact])
                triple = (subjectRDF, predicateRDF, objectRDF)
                g.add(triple)

                # NationalBudgetFactInstance has type TransactionalRealisationPhase, e.g. fbud:BFCO30720-2033 fbud:hasTransactionalRealisationPhase fbud:Commitment
                TransactionalRealisationPhaseInstance = 'Commitment'
                subjectRDF =  URIRef(fbud[NationalBudgetFactInstance])
                predicateRDF = URIRef(fbud[hasTransactionalRealisationPhase])
                objectRDF = URIRef(fbud[TransactionalRealisationPhaseInstance])
                triple = (subjectRDF, predicateRDF, objectRDF)
                g.add(triple)

                # NationalBudgetFactInstance has Monetary Amount, e.g. fbud:BFCO30720-2033 ffact:hasMonetaryAmount ffact:MACO30720-2032
                MonetaryAmountInstance = MonetaryAmountPrefix + "CO" + UniqueIdentifier + "-" + str(line_count) + '-' + NationalBudgetFactYear
                subjectRDF =  URIRef(fbud[NationalBudgetFactInstance])
                predicateRDF = URIRef(ffact[hasMonetaryAmount])
                objectRDF = URIRef(ffact[MonetaryAmountInstance])
                triple = (subjectRDF, predicateRDF, objectRDF)
                g.add(triple)

                # NationalBudgetFactInstance has National Budget Period, e.g. fbud:BFCO30720-2033 fbud:hasNationalBudgetPeriod fstr:BP2019
                NationalBudgetFactPeriodInstance = NationalBudgetFactPeriodPrefix + NationalBudgetFactYear
                subjectRDF =  URIRef(fbud[NationalBudgetFactInstance])
                predicateRDF = URIRef(fbud[hasNationalBudgetFactPeriod])
                objectRDF = URIRef(fstr[NationalBudgetFactPeriodInstance])
                triple = (subjectRDF, predicateRDF, objectRDF)
                g.add(triple)

                # NationalBudgetFactInstance has Transactional Direction, e.g. fbud:BFCO30720-2033 fbud:hasTransactionalDirection fbud:Outgoing
                TransactionalDirection = TransactionalDirectionDict[dfibos["UITG_ONTV_IND"][line_count]]
                subjectRDF =  URIRef(fbud[NationalBudgetFactInstance])
                predicateRDF = URIRef(fbud[hasTransactionalDirection])
                objectRDF = URIRef(fbud[TransactionalDirection])
                triple = (subjectRDF, predicateRDF, objectRDF)
                g.add(triple)

                # Monetary Amount has Currency, e.g. ffact:MACO30720-2032 ffact:hasCurrency ffact:EUR
                subjectRDF =  URIRef(ffact[MonetaryAmountInstance])
                predicateRDF = URIRef(ffact[hasCurrency])
                objectRDF = URIRef(ffact[CurrencyInstance])
                triple = (subjectRDF, predicateRDF, objectRDF)
                g.add(triple)

                # Monetary Amount has Value, e.g. ffact:MACO30720-2032 ffact:hasValue "580290"^^xsd:integer
                subjectRDF =  URIRef(ffact[MonetaryAmountInstance])
                predicateRDF = URIRef(ffact[hasValue])
                objectRDF = Literal(int(item[1]),datatype=XSD.integer)
                triple = (subjectRDF, predicateRDF, objectRDF)
                g.add(triple)

    line_count = line_count + 1
print("Done processing rows")
print("now serializing graph and writing to file...")

print (g.serialize(destination='Script Output/outputmutations.ttl',format='turtle'))

print("Done serializing graph and writing to file")

conn.close()     #<--- Close the connection

print("End of program")
