# Financial Function data models

This is the repository for any development surrounding the financial function of the Dutch government using knowledge graph technology.

## 1. RDF-generating scripts

### 1.1 Python-based scripts

#### 1.1.1 `Budgetstructure.py`

This generates the budget structure for the Dutch government as RDF triples, based on a relational table of budget structure elements as stored in IBOS - the interdepartmental budgetary system of the Dutch government.

#### 1.1.2 `ProgramPolicyIndicator.py`

This generates RDF-triples capturing whether a budget structure is (a) program or overhead related and (b) policy or non-policy related.

#### 1.1.3 `BudgetMutations.py`

This generates the budget mutations as RDF-triples for the Dutch government based on a relational table of budget mutations.

#### 1.1.4 `BudgetingBalancePhase.py`

This generates the different recognized budgeting balance phases based on a CSV-file, that can be used as a reference to calculate the balance per a certain phase. The CSV-file is maintained by hand and is experimental. It is based on different selections of budget source documents and time.

#### 1.1.5 `LegacyBudgetaryAmounts.py`

This generates budgetary amounts for budget balance sheets in the past. This solves the problem of having to report 4 years back in time, but on current budget structure elements that do not exist in the past. At the moment highly experimental and not yet validated.

### 1.2 SQL scripts to query the budgetary system

#### 1.2.1 `Begrotingsonderdelen_niveau_naam.sql`

This retrieves part of the budget structure for the Dutch government based on a relational table of budget structure elements as stored in IBOS - the interdepartmental budgetary system of the Dutch government. This query concerns the entire budget structure hierarchy and describing elements within the hiearcy, except the metadata for budget chapters.

#### 1.2.2 `lijst_begrotingen.sql`

This retrieves part of the budget structure for the Dutch government based on a relational table of budget structure elements as stored in IBOS - the interdepartmental budgetary system of the Dutch government. This query concerns the budget chapters.

## 2. Static ontologies

The static ontologies are created and maintained in the [Turtle 1.1]() format.

### 2.1 `OntologyBudgetStructureElements.ttl`

This captures the model of a budget structure element for the Dutch government.

### 2.2 `OntologyNationalBudgetFact.ttl`

This captures the model of a budget mutation for the Dutch government.

### 2.3 `OntologyFinancialFact.ttl`

This captures the essence of a financial fact. Meant to be a generic, reusable and simple ontology. May be deprecated or transformed in the future.

### 2.4 `PresentationOntologyFull.ttl`

This captures the model for a information product within the Dutch government, including the representation of a visual table containing information and layout.

### 2.5 `OntologyBudgetPolicyAndTheme.ttl`

This model captures the budget policies and themes connected with budget structure elements. For instance, article 31 of the Justice and Security Department is about financing the National Police. This ontology captures the very goal of this financing ("A safe society with the help of a properly functioning police organization.")

### 2.6 `Rijksbegrotingsvoorschriften-Model.ttl`

This model captures the regulations as presented on rbv.rijksfinancien.nl, which regulates how budget bills, annual reports should look like and what information they should contain. At the moment this model only contains the initial budget bill and memorandum of clarification.

### 2.7 `Rijksbegrotingsvoorschriften-Data.ttl`

Fictionalised data for one initial budget bill and memorandum of clarification.

### 2.8 `BudgetingCycle.ttl`

This model captures the different recognized budgeting balance phases, that can be used as a reference to calculate the balance per a certain phase. It is based on different selections of budget source documents and time. At the moment still experimental and unvalidated.

### 2.9 `Comptabiliteitswet.ttl`

This model captures one of the most fundamental laws within the national budget cycle, the Comptabiliteitswet 2016. The law describes, among other things, regulations on governmental budget management, the national budget cycle and the accountability of the government budget process.

### 2.10 `NationalBudgetProcess.ttl`

This model captures part of the constitution of The Netherlands that describes the role of the King, Ministers in governmental policy and accountability. May be deprecated in the future and merged with the ontologies regarding the Comptabiliteitswet and the budget cycle phases.

## 3. Validation & publication script

The validation and publication script publishes a new version of the Financial Function models to the Ministry of Finance triple store.

### 3.1 Install JavaScript/TypeScript support

Install [Node.js](https://nodejs.org) and [Yarn](https://yarnpkg.com) on your system:

- Node.js is the runtime that allows your to run JavaScript (and thus TypeScript) code outside of a web browser.
- Yarn is a modern package manager for handling JavaScript/TypeScript dependencies.

### 3.2 Configure API Token

For secure access to the triple store environment, configure an API Token for the [FinancialFunction bot](https://fin.triply.cc/FinancialFunction-bot).

### 3.3 Install dependencies

Run the following command to install the JavaScript/TypeScript dependencies:

```sh
yarn
```

### 3.4 Build

Run the following command to generate JavaScript code from the corresponding TypeScript code:

```sh
yarn build
```

### 3.5 Run

Run the following command to validate and publish the Financial Function data models:

```sh
yarn ratt ./lib/publish.js
```

## 4. Snippets

The Snippets section of this Gitlab project show two examples of SPARQL queries used in the process to define and generate formal budgerary balance sheets for the Dutch government.

1. The first snippet is the construct query to model both the HTML-snippets and the datacube collection of observations;
2. The second snippet is the select query to retrieve the information generated throught the construct query, fill it dynamically and cpmpose the resulting HTML object on the fly.

**HOW TO RUN THE SCRIPTS (minimal version):**

First run the Budget Structure script. Make sure you have a download of the budget structure elements from the Dutch budgetary application (IBOS) in the folder to be processed. The Budget Structure script outputs a couple of files:
            1. Budget structure index file
            2. Budget structure hierachy index file
            3. Budget Structure ttl-file

You can upload the Budget Structure ttl-file in a triple store so that you can query it. The Budget Structure Index file is needed when running the script Budget Mutation.

Second run the ProgramPolicyIndicator script. Make sure you have the Budget Structure Index file and a download of the budget mutations from the Dutch budgetary application (IBOS) in the folder to be processed. The ProgramPolicyIndicator script outputs the following file:
            1. OutputstructureIndicators ttl-file

You can upload the OutputstructureIndicators ttl-file in a triple store so that you can query it.

Third run the Budget Mutation script. Make sure you have the Budget Structure Index file and a download of the budget mutations from the Dutch budgetary application (IBOS) in the folder to be processed. The Budget Mutations outputs the following file:
            1. Budget Mutations ttl-file

You can upload the Budget Mutations ttl-file in a triple store so that you can query it.
